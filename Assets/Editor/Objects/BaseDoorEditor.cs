﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BaseDoor))]
public class BaseDoorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        BaseDoor myTarget = (BaseDoor)target;
        if (GUILayout.Button("Save OPEN Door Position"))
        {
            myTarget.SaveOpenDoorPosition();
        }
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        if (GUILayout.Button("Save CLOSE Door Position"))
        {
            myTarget.SaveCloseDoorPosition();
        }

        DrawDefaultInspector();
    }

}
