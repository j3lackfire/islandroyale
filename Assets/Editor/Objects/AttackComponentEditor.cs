﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(AttackComponent))]
public class AttackComponentEditor : Editor {

    void OnSceneGUI()
    {
        AttackComponent attackComponent = (AttackComponent)target;
        BaseObject targetObject = attackComponent.GetTargetObject();
        if (targetObject != null)
        {
            Handles.color = Color.red;
            Handles.DrawLine(attackComponent.transform.position, targetObject.transform.position);
        }
    }
}
