﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MoveComponent))]
public class MoveComponentEditor : Editor {

    void OnSceneGUI()
    {
        MoveComponent moveComponent = (MoveComponent)target;
        if (moveComponent.EditorShouldMoveSomeWhere())
        {
            Handles.color = Color.green;
            Handles.DrawLine(moveComponent.transform.position, moveComponent.EditorTargetPosition());
        }
    }
}
