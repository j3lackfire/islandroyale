﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BaseBullet))]
public class BaseBulletEditor : Editor {
    void OnSceneGUI()
    {
        BaseBullet baseBullet = (BaseBullet)target;
        Handles.color = Color.white;
        Handles.DrawLine(baseBullet.GetBulletStartPos(), baseBullet.GetBulletStartPos() + baseBullet.GetBulletDirection() * baseBullet.bulletLength);
    }

}
