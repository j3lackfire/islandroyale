﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BaseWeapon))]
public class BaseWeaponEditor : Editor
{

    public override void OnInspectorGUI()
    {
        BaseWeapon myTarget = (BaseWeapon)target;

        if (GUILayout.Button("Save weapon pos"))
        {
            myTarget.EditorSaveWeaponPos();
        }
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        DrawDefaultInspector();

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        if (GUILayout.Button("Set weapon pos"))
        {
            myTarget.EditorSetWeaponPos();
        }
    }
}