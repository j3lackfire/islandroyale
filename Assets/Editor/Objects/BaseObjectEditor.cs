﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(BaseObject))]
public class BaseObjectEditor : Editor {

    void OnSceneGUI()
    {
        BaseObject baseObject = (BaseObject)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(baseObject.transform.position, Vector3.up, Vector3.forward, 360, baseObject.sight);
    }
}
