﻿Shader "Custom/HighLightColor" 
{
	Properties
	{
		_Color("MyColor", Color) = (1.0, 1.0, 1.0, 1.0)
		_MinAlphaValue("Min alpha", Range(0, 255)) =0
		_MaxAlphaValue("Max alpha", Range(0, 255)) = 50
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
		}
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert alpha
		fixed4 _Color;
		float _MinAlphaValue;
		float _MaxAlphaValue;
		// Note: pointless texture coordinate. I couldn't get Unity (or Cg)
		//to accept an empty Input structure or omit the inputs.
		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o)
		{
			o.Albedo = _Color.rgb;
			o.Emission = _Color.rgb; // * _Color.a;
			//o.Alpha = _Color.a;
			//o.Alpha = _MinAlphaValue / 255 + ((_CosTime.w + 1) / 2) * _MaxAlphaValue / 255;
			o.Alpha = _MinAlphaValue / 255 + ((cos(_Time.z) + 1)/ 2) * _MaxAlphaValue / 255;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
