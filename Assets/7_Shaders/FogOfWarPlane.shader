﻿//Fog of war plane with a Stencil mask 
Shader "Custom/FogOfWarPlane" {
	Properties
	{
		_Color("MyColor", Color) = (1.0, 1.0, 1.0, 1.0)
	}

	SubShader{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
		}
		LOD 200

		Stencil
		{
			Ref 1
			Comp NotEqual
		}


		CGPROGRAM
		#pragma surface surf Lambert alpha
		fixed4 _Color;

		// Note: pointless texture coordinate. I couldn't get Unity (or Cg)
		//to accept an empty Input structure or omit the inputs.
		struct Input 
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o) 
		{
			o.Albedo = _Color.rgb;
			o.Emission = _Color.rgb; // * _Color.a;
			o.Alpha = _Color.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}