﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class BuildingRenderer : MonoBehaviour {

    private BaseBuilding baseBuilding;

    private Renderer opaqueRenderer;
    private Renderer transparentRenderer;
    private List<Renderer> wallsRenderer;
    private List<Renderer> othersRenderers;
    //everything inside the building
    private List<Renderer> interiorRenders;

    [ReadOnly][SerializeField]
    private Material opaqueMaterial;

    [ReadOnly][SerializeField]
    private Material transparentMaterial;

    public void Init(BaseBuilding _baseBuilding, Transform _transparentRenderer, Transform _wall, Transform _interiors, Transform _otherRenderers)
    {
        baseBuilding = _baseBuilding;
        opaqueRenderer = GetComponent<Renderer>();
        transparentRenderer = _transparentRenderer.GetComponent<Renderer>();
        wallsRenderer = new List<Renderer>();
        wallsRenderer.AddRange(_wall.GetComponentsInChildren<Renderer>());

        interiorRenders = new List<Renderer>();
        if (_interiors != null)
        {
            interiorRenders.AddRange(_interiors.GetComponentsInChildren<Renderer>());
        }

        othersRenderers = new List<Renderer>();
        if (_otherRenderers != null)
        {
            othersRenderers.AddRange(_otherRenderers.GetComponentsInChildren<Renderer>());
        }
        opaqueMaterial = opaqueRenderer.sharedMaterial;
        transparentMaterial = transparentRenderer.sharedMaterial;
    }

    public void ShowBuildingOpaque()
    {
        opaqueRenderer.shadowCastingMode = ShadowCastingMode.On;
        transparentRenderer.enabled = false;
        foreach(Renderer r in wallsRenderer)
        {
            r.enabled = false;
        }
        foreach (Renderer r in interiorRenders)
        {
            r.enabled = false;
        }
        foreach (Renderer r in othersRenderers)
        {
            r.sharedMaterial = opaqueMaterial;
        }
    }

    public void ShowBuildingTransparent()
    {
        opaqueRenderer.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
        transparentRenderer.enabled = true;
        foreach (Renderer r in wallsRenderer)
        {
            r.enabled = true;
        }
        foreach(Renderer r in interiorRenders)
        {
            r.enabled = true;
        }
        foreach (Renderer r in othersRenderers)
        {
            r.sharedMaterial = transparentMaterial;
        }
    }
}
