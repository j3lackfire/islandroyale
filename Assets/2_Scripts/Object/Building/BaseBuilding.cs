﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBuilding : MonoBehaviour {

    [ReadOnly][SerializeField]
    private BuildingRenderer buildingRenderer;
    private UnderLOSCheck losCheck;

    //change to init later if needed to :/
    void Awake()
    {
        buildingRenderer = transform.Find("OpaqueRenderer").GetComponent<BuildingRenderer>();
        buildingRenderer.Init(this, transform.Find("TransparentRenderer"), transform.Find("Walls"), transform.Find("Interiors"),transform.Find("OthersRenderer"));

        buildingRenderer.ShowBuildingOpaque();

        losCheck = transform.GetComponentInDirectChildren<UnderLOSCheck>();
        losCheck.OnObjectReveal = buildingRenderer.ShowBuildingTransparent;
        losCheck.OnObjectHidden = buildingRenderer.ShowBuildingOpaque;
    }
}
