﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//make this into part of the weapon manager
[SelectionBase]
public class PickUpWeapon : InteractableObject {

    public WeaponType weaponType;
    private Renderer weaponRenderer;
    private UnderLOSCheck losCheck;

    [SerializeField]
    private int ammoCount;

    protected override void OnEnable()
    {
        base.OnEnable();
        weaponRenderer = GetComponentInChildren<Renderer>();
        
        losCheck = transform.GetComponentInChildren<UnderLOSCheck>();
        losCheck.OnObjectReveal = () => {
            weaponRenderer.enabled = true;
        };
        losCheck.OnObjectHidden = () => {
            weaponRenderer.enabled = false;
        };

        weaponRenderer.enabled = losCheck.IsBeingSeen();
    }

    public void RandomizedPosition()
    {
        float randomRotation = Random.Range(0, 360f);
        Vector3 currentRotation = weaponRenderer.transform.localEulerAngles;
        weaponRenderer.transform.localRotation = Quaternion.Euler(new Vector3(currentRotation.x, randomRotation, currentRotation.z));
        losCheck.transform.localRotation = Quaternion.Euler(new Vector3(0, randomRotation, 0f));
    }

    public void SetAmmo(int ammo) { ammoCount = ammo; }

    public void SetPosition(Vector3 v) { transform.position = v; }

    private void RemovePickUp()
    {
        Director.instance.weaponManager.RemovePickUp(this);
        ReturnToPool();
        OnExitWith(interactiveComponent);
    }

    public override void InteractiveFunction(InteractiveComponent user)
    {
        base.InteractiveFunction(user);
        //just in case I want to leave all the component as children object
        WeaponUserComponent weaponUser = user.GetBaseObject().gameObject.GetComponent<WeaponUserComponent>();
        if (weaponUser != null && weaponUser.CanPickUpWeapon())
        {
            weaponUser.EquipWeapon(weaponType, ammoCount);
            RemovePickUp();
        } else
        {
            Debug.Log("<color=red>Can't pick up weapon </color>" + user.gameObject.name);
        }
    }
}
