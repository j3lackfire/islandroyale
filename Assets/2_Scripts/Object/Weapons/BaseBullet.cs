﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBullet : PooledObject {

    private LineRenderer lineRenderer;
    private BulletManager bulletmanager;
    private AttackComponent attacker;
    private BaseWeapon baseWeapon;
    [ReadOnly]
    [SerializeField]
    private Vector3 startPos;

    [ReadOnly]
    [SerializeField]
    private Vector3 bulletDirection;

    public float bulletLength = 2f;
    public float maxTravelDistance = 50f;
    public float bulletSpeed = 5f;
    public int damage;

    //cached value
    [ReadOnly][SerializeField]
    private float currentDistanceTravel;

    private bool shoudRemoveBullet;
    private bool removeBulletNextFrame;

    public void Init(AttackComponent _attacker, BaseWeapon _baseWeapon, Vector3 _startPos, Vector3 _endPos)
    {
        if (lineRenderer == null)
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
        //assigning values
        bulletmanager = Director.instance.bulletManager;
        attacker = _attacker;
        baseWeapon = _baseWeapon;
        damage = baseWeapon.damage;
        bulletLength = baseWeapon.bulletLength;
        bulletSpeed = baseWeapon.bulletSpeed;
        maxTravelDistance = baseWeapon.travelDistance;
        startPos = _startPos;
        bulletDirection = new Vector3(_endPos.x - _startPos.x, 0f, _endPos.z - _startPos.z).normalized;

        startPos = _startPos - bulletDirection * bulletLength * Time.deltaTime;

        lineRenderer.SetPosition(0, startPos);
        lineRenderer.SetPosition(1, startPos + bulletDirection * bulletLength);
        currentDistanceTravel = 0f;
        shoudRemoveBullet = false;
        removeBulletNextFrame = false;

        //GameObject g = new GameObject();
        //g.transform.position = _startPos;
        //GameObject f = new GameObject();
        //f.transform.position = _startPos + bulletDirection * bulletLength;
        //Debug.Break();
    }

    public void DoUpdate()
    {
        if (!shoudRemoveBullet && currentDistanceTravel < maxTravelDistance)
        {
            BaseObject hitObject;
            if (LOSHelper.CheckBulletHit(startPos, bulletDirection, bulletLength, out hitObject))
            {
                if (hitObject != null)
                {
                    hitObject.OnDamageRecieve(damage, attacker);
                } else
                {
                    //do nothing, destroy bullet, effect something ?
                }
                shoudRemoveBullet = true;
            }
        } else
        {
            shoudRemoveBullet = true;
        }

        startPos += bulletDirection * bulletSpeed * Time.deltaTime;
        lineRenderer.SetPosition(0, startPos);
        lineRenderer.SetPosition(1, startPos + bulletDirection * bulletLength);
        currentDistanceTravel += bulletSpeed * Time.deltaTime;
        //doing this instead of remove the bullet the moment it reach distance
        //this will cause the bullet to travel a little but further, seem more
        //realistitc
        if (shoudRemoveBullet)
        {
            if (!removeBulletNextFrame)
            {
                removeBulletNextFrame = true;
            } else
            {
                DestroyBullet();
                return;
            }
        }
    }

    private void DestroyBullet()
    {
        bulletmanager.RemoveBullet(this);
        ReturnToPool();
    }

    public Vector3 GetBulletStartPos() { return startPos;}

    public Vector3 GetBulletDirection() { return bulletDirection; }
}
