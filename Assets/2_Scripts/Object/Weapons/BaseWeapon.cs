﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseWeapon : PooledObject {

    [Header("Values and components")]
    private BulletManager bulletmanager;

	[ReadOnly][SerializeField]
	protected WeaponUserComponent weaponUser;

	[ReadOnly][SerializeField]
	protected Transform equippingHand;
    
	[ReadOnly][SerializeField]
    protected Vector3 weaponPosition;
    
	[ReadOnly][SerializeField]
    protected Vector3 weaponRotation;

    [SerializeField]
    //the point where the bullet comes from
    private Transform bulletStartPos;
    private ParticleSystem muzzleFlash;

    private Renderer weaponRenderer;

	[Header("Weapons variables - set in editor")]
	public int damage;
    public float bulletLength = 2f;
    public float bulletSpeed = 80f;
    public float travelDistance = 75f;
    public float range;
	public float accuracy;

	public float rateOfFire; //this is attack duration
    public float dealDamageTime;

    [Header("Ammo values")]
    [SerializeField]
    private WeaponType weaponType;

    [SerializeField]
    private AmmoType usedAmmoType;
    [ReadOnly][SerializeField]
    private int ammoCount = 0;

    public VoidDelegate OnTriggerPulled;

    public void InitWeapon(WeaponUserComponent _weaponUser, Transform _equippingHand, int _ammoCount)
    {
        bulletmanager = Director.instance.bulletManager;
        weaponRenderer = GetComponent<Renderer>();
        OnTriggerPulled = () => { };

        ammoCount = UseAmmo() ? - 1 : _ammoCount;

        bulletStartPos = transform.Find("Bullet_Star_Pos");
        Transform muzzleFlashTransform = transform.Find("Muzzle_Flash");
        muzzleFlash = muzzleFlashTransform == null ? null : muzzleFlashTransform.GetComponent<ParticleSystem>();

        weaponUser = _weaponUser;
		equippingHand = _equippingHand;
		transform.parent = equippingHand;
		transform.localScale = Vector3.one;
		transform.localPosition = weaponPosition;
		transform.localRotation = Quaternion.Euler(weaponRotation);
    }

    public void AddAmmo(int ammoNumber) { ammoCount += ammoNumber; }

    public int GetAmmoCount() { return GetAmmoType() == AmmoType.No_Ammo ? -1 : ammoCount; }

    public AmmoType GetAmmoType() { return usedAmmoType; }

    public WeaponType GetWeaponType() { return weaponType; }

	public void UnEquipWeapon() 
	{
		transform.parent = null;
		ReturnToPool();
	}

    public bool CanPullTrigger() { return usedAmmoType == AmmoType.No_Ammo || ammoCount > 0; }

    public bool UseAmmo() { return usedAmmoType == AmmoType.No_Ammo || usedAmmoType == AmmoType.Invalid; }

    //pool the trigger to attack a position
    public void PullTrigger(BaseObject targetObject, bool canAttackFriendly)
    {
        if (usedAmmoType != AmmoType.No_Ammo)
        {
            ammoCount--;
        }
        //instant damage, no bullet
        if (bulletSpeed <= 0)
        {
            targetObject.OnDamageRecieve(damage < 0 ? weaponUser.baseDamage : damage, weaponUser);
        } else
        {
            Vector3 targetPosition = targetObject.transform.position;
            Vector3 bulletDirection = new Vector3(targetPosition.x - bulletStartPos.position.x, 0f, targetPosition.z - bulletStartPos.position.z).normalized;
            Vector3 bulletFinalPosition = bulletStartPos.position + bulletDirection * range;
            Vector3 randomizedPosition = bulletFinalPosition + 4 * (1 - accuracy) * UnityEngine.Random.Range(-1f, 1f) * Vector3.one;
            Vector3 myBulletStartPos = new Vector3(bulletStartPos.position.x, 2.5f, bulletStartPos.position.z);

            bulletmanager.SpawnBullet(weaponUser, this, myBulletStartPos, randomizedPosition);
            muzzleFlash.Play();
        }
        if (OnTriggerPulled != null)
        {
            OnTriggerPulled();
        } else
        {
            Debug.Log("<color=red>The on trigger pull is null, don't fucking know why :/ </color>");
        }
    }

    public Renderer GetWeaponRenderer() { return weaponRenderer; }

#if UNITY_EDITOR
    //function to fast cache weapons position and rotation, set in editor only
    public void EditorSaveWeaponPos()
    {
        weaponPosition = transform.localPosition;
        weaponRotation = transform.localRotation.eulerAngles;
    }

    public void EditorSetWeaponPos()
    {
        transform.localPosition = weaponPosition;
        transform.localRotation = Quaternion.Euler(weaponRotation);
    }
#endif

}
