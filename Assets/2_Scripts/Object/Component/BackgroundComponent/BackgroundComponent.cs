﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * used as a background only, will never wake up
 * The component will always run the background action function
 * because it is always checked by the ComponenCheck function.
 * because background has a really high update, it will always be called.
 * Sealed these functions up so I don't do anything with them later
*/
public class BackgroundComponent : MonoBehaviour {
    protected BaseObject baseObject;
    protected TeamManager teamManager;

    public virtual void InitComponent(BaseObject _baseObject, TeamManager _teamManager)
    {
        baseObject = _baseObject;
        teamManager = _teamManager;

        baseObject.AddComponentTypeToList(GetComponentType());
    }

    //don't ever touch this function
    //all background component will not use this function
    public virtual void ComponentUpdate() {}

    public BaseObject GetBaseObject() { return baseObject; }

    public virtual ComponentType GetComponentType() { return ComponentType.Invalid; }
}
