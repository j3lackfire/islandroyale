﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//interactive component is capable of 
public class InteractiveComponent : BackgroundComponent {
    [ReadOnly][SerializeField]
    private InteractableObject currentInteractingObject;
    
    //when base object touch the interactable object hit-box
    public VoidDelegateInteractbleObject OnEnter;

    //on the object use the interactable object, active it function or whatever
    public VoidDelegateInteractbleObject OnUse;

    //on the object leave the interacable object hit-box range
    public VoidDelegateInteractbleObject OnExit;

    public override void InitComponent(BaseObject _baseObject, TeamManager _teamManager)
    {
        base.InitComponent(_baseObject, _teamManager);
        currentInteractingObject = null;
        OnEnter = (InteractableObject i) => 
        {
            currentInteractingObject = i;
        };

        OnUse = (InteractableObject i) => {
            if (i != null)
            {
                i.InteractiveFunction(this);
            }
        };
        OnExit = (InteractableObject i) => 
        {
            if (currentInteractingObject == i)
            {
                currentInteractingObject = null;
            }
        };
    }

    public void UseObject()
    {
        OnUse(currentInteractingObject);
    }

    public InteractableObject GetCurrentInteractingObject() { return currentInteractingObject; }

    public override ComponentType GetComponentType() { return ComponentType.InteractiveComponent; }
}
