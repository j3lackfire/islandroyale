﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfHealComponent : BackgroundComponent {

    [SerializeField]
    private float healthRegenRate = 1f; //1%
    [SerializeField]
    private float healingDelayWhenDamaged = 3f; //

    //temp variable
    protected float healthRegenCountUp = 0f;

    public override void InitComponent(BaseObject _baseObject, TeamManager _teamManager)
    {
        base.InitComponent(_baseObject, _teamManager);
        healthRegenCountUp = 0f;
        baseObject.OnDamageRecieve += OnObjectReceiveDamage;
    }

    public override void ComponentUpdate()
    {
        base.ComponentUpdate();

        if (CanRegenHealth())
        {
            RegenHealth();
        }
    }

    protected void OnObjectReceiveDamage(int damage, AttackComponent attackComponent)
    {
        healthRegenCountUp = healingDelayWhenDamaged;
    }

    protected bool CanRegenHealth()
    {
        if (healthRegenCountUp < 0f)
        {
            healthRegenCountUp += Time.deltaTime;
            if (healthRegenCountUp >= 0f)
            {
                return true;
            }
            return false;
        } else
        {
            return true;
        }
        
    }

    protected virtual void RegenHealth()
    {
        if (healthRegenRate <= 0f 
            || baseObject.GetObjectState() == ObjectState.Die 
			|| baseObject.GetHealth() >= baseObject.GetMaxHealth())
        {
            return;
        }
		healthRegenCountUp += Time.deltaTime * healthRegenRate * baseObject.GetMaxHealth() / 100f;
        if (healthRegenCountUp >= 1)
        {
			 baseObject.OnHealthChange((int)healthRegenCountUp + baseObject.GetHealth());
            healthRegenCountUp -= (int)healthRegenCountUp;
        }
    }

    public override ComponentType GetComponentType() { return ComponentType.SelfHealComponent; }

}
