﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class SelectableObject : BackgroundComponent {

    private HighLightManager highLightManager;
	private InputManager inputManager;

    //maybe set it to bool.
    //if object CAN be selected, return true, else, return false
    public VoidDelegate OnObjectSelected;
	public VoidDelegate OnObjectDeselected;
    
	public VoidDelegate OnObjectStopped;

    public VoidDelegate OnObjectDoubleClicked = () => { };
    public VoidDelegate OnObjectRightClicked = () => { };

	public VoidDelegateVector3 OnRightClickOrder;
	public VoidDelegateVector3 OnDragOrder = (Vector3 pos) => { };

	public VoidDelegateBaseObject OnRightClickTarget;

    //somehow, chain a manager to this
    public override void InitComponent(BaseObject _baseObject, TeamManager _teamManager)
    {
        base.InitComponent(_baseObject, _teamManager);
        gameObject.layer = LayerMask.NameToLayer("SelectableObject");
        highLightManager = Director.instance.highLightManager;
		inputManager = Director.instance.inputManager;

        OnObjectSelected = () => { };
        OnObjectDeselected = () => { };

        OnObjectStopped = () => { };

        OnRightClickOrder = (Vector3 v) => { };
        OnRightClickTarget = (BaseObject b) => { };

        //---------------------

        OnObjectSelected += ObjectSelected;
        OnObjectDeselected += ObjectDeselected;

        OnObjectStopped += ObjectStopped;

        OnRightClickOrder += RightClickPosition;
        OnRightClickTarget += RightClickTarget;

		baseObject.OnStateChange += OnStateChange;
    }

    private void ObjectSelected() { highLightManager.SelectObject(this); }

    private void ObjectDeselected() { highLightManager.DeslectObject(this); }

    private void ObjectStopped() { baseObject.SetIdle(); }

    private void RightClickPosition(Vector3 position) 
	{ 
		baseObject.OnPositionTargetted(position, true); 
	}

	private void OnStateChange(ObjectState s) {
		if (s == ObjectState.Die) 
		{
			ObjectDeselected();
			inputManager.DeselectObject(this);
		}
	}

    private void RightClickTarget(BaseObject target)
    {
		if (teamManager.IsObjectFriendly(target))
        {
			baseObject.OnFriendlyObjectTargetted(target, true);
        } else
        {
			baseObject.OnEnemyObjectTargetted(target, true);
        }
    }

    public override ComponentType GetComponentType() { return ComponentType.SelectableObject; }

}
