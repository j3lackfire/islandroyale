﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//object with this component can choose to NOT attack an enemy even if the enemy is in range
[Serializable]
[RequireComponent(typeof(AttackComponent))]
public class CalmComponent : BackgroundComponent {

    //remain calm = not attack anything
    [ReadOnly][SerializeField]
    private bool isBeingCalm = true;

    private AttackComponent attackComponent;

    public override void InitComponent(BaseObject _baseObject, TeamManager _teamManager)
    {
        base.InitComponent(_baseObject, _teamManager);
        isBeingCalm = true;

        attackComponent = baseObject.GetComponent<AttackComponent>();

        attackComponent.DefaultActiveCondition = () =>
        {
            return (!isBeingCalm && attackComponent.ShouldActiveComponent());
        };

        attackComponent.ActiveCondition = attackComponent.DefaultActiveCondition;
    }

    public void SetAgressive() { isBeingCalm = false; }

    public void SetCalm() { isBeingCalm = true; }

    public void SwitchCalmState() { isBeingCalm = !isBeingCalm; }

    public bool IsBeingCalm() { return isBeingCalm; }

    public override ComponentType GetComponentType() { return ComponentType.CalmComponent; }
}
