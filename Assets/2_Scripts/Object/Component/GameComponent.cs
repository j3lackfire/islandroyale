﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BaseObject))]
public class GameComponent : MonoBehaviour
{
    [ReadOnly][SerializeField]
    private int priority;

    protected BaseObject baseObject;
    protected TeamManager teamManager;
    protected ObjectRenderer objectRenderer;
    //the priority order is the order tell explicit by the manager or the user.
    [ReadOnly][SerializeField]
    protected bool isExplicitOrder; //can the current action be interupt by another action.

	//if return true, this component should be actived
	public BoolDelegate ActiveCondition;
	//if return true, this component should be stopped
	public BoolDelegate StoppingCondition;

    public BoolDelegate DefaultActiveCondition;
    public BoolDelegate DefaultStoppingCondition;

    public virtual void InitComponent(BaseObject _baseObject, ObjectRenderer _objectRenderer, TeamManager _teamManager)
    {
        baseObject = _baseObject;
        teamManager = _teamManager;
        objectRenderer = _objectRenderer;
        priority = ComponentPriority.GetPriority(GetType());
        isExplicitOrder = false;

        DefaultActiveCondition = ShouldActiveComponent;
        DefaultStoppingCondition = ShouldStopComponent;

		ActiveCondition = DefaultActiveCondition;
		StoppingCondition = DefaultStoppingCondition;

        baseObject.AddComponentTypeToList(GetComponentType());
    }

	//check should the component be active or stop
	public virtual bool ShouldActiveComponent() { return false; }

	public virtual bool ShouldStopComponent() { return false; }

    //The update function
    public virtual void ComponentUpdate() { }

	public virtual void RegisterComponent<T>(T firstParameter, BoolDelegate activeCondition, BoolDelegate stoppingCondition, bool _isManagerCall) {
		if(activeCondition == null) {
			ActiveCondition = DefaultActiveCondition;
		} else {
			ActiveCondition = activeCondition;
		}

		if(stoppingCondition == null) {
			StoppingCondition = DefaultStoppingCondition;
		} else {
			StoppingCondition = stoppingCondition;
		}

		isExplicitOrder = _isManagerCall;

        baseObject.OnStateChange += (ObjectState s) =>
        {
            if (s == ObjectState.Die)
            {
                ForceStopComponent();
            }
        };
	}

    //When the component is active
    public virtual void SetAsActiveComponent() { baseObject.OnComponentActive(GetType()); }

    //When the component finish it job and let the base object back to idle
    public virtual void OnComponentFinished()
    {
        baseObject.OnComponentFinished(GetType());
        //isExplicitOrder = false;
    }

    //Stop the component
	public virtual void ForceStopComponent() { OnComponentFinished();}

    protected void PlayAnimation() { objectRenderer.PlayAnimation(GetType()); }

    //get the priority of the component
    public int GetPriority() { return priority; }

    //check if current component is being active
    protected bool IsBeingActive() { return baseObject.GetCurrentActiveComponent() == null ? false : baseObject.GetCurrentActiveComponent().GetType() == GetType();  }

    public bool CanBeInterupted() { return !isExplicitOrder; }

    protected void ForceStartComponent() { baseObject.ForceStopComponentExcept(GetType()); }

    public BaseObject GetBaseObject() { return baseObject; }

    public virtual ComponentType GetComponentType() { return ComponentType.Invalid; }
}

