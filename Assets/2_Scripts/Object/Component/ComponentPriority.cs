﻿using UnityEngine;
using System;

//config file :/
public static class ComponentPriority {

    public static int GetPriority(Type t)
    {
        string componentType = t.ToString();
        switch (componentType)
        {
            case "AttackComponent":
            case "WeaponUserComponent":
                return 100;
            case "MoveComponent":
                return 1;
            default:
                Debug.LogError("Component not defined " + componentType);
                return 0;
        }
        
    }
}

public enum ComponentPriorityEnum
{
    Background = 1000, //always running and check in the background. Like, health regen, aura stuff
    //The background component does not 
    None = -1,//can't even override the Idle state
    Weak = 0, //it can overide the Idle state
    Medium = 100, //can overide the weak component
    High = 200 //highest, for now
}

public enum ComponentType
{
    Invalid = -1,
    MoveComponent = 0,
    AttackComponent = 1,
    WeaponUserComponent = 2,
    //.....
    CalmComponent = 100,
    SelectableObject = 101,
    SelfHealComponent = 102,
    InteractiveComponent = 103
}