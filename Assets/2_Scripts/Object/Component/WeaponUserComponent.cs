﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponUserComponent : AttackComponent {

    public VoidDelegateBaseWeapon OnWeaponEquipped;

	private HumanoidRenderer humanoidRenderer = null;
    private WeaponManager weaponManager;

    private float baseRange = -1f;
    //private float baseAgressiveRange = -1f;
    private float baseAttackDuration = -1f;
    private float baseDealDamageTime = -1f;

    [Header("Weapon user component details")]
    //set by the editor
    [SerializeField]
    private WeaponType currentWeaponType;

	[ReadOnly][SerializeField]
	private BaseWeapon currentEquippingWeapon;

	public override void InitComponent (BaseObject _baseObject, ObjectRenderer _objectRenderer, TeamManager _teamManager)
	{
        base.InitComponent(_baseObject, _objectRenderer, _teamManager);
        weaponManager = Director.instance.weaponManager;
        OnWeaponEquipped = (BaseWeapon b) => { };

        if (baseRange <= -1f)
        {
            baseRange = attackRange;
            //baseAgressiveRange = agressiveRange;
            baseAttackDuration = attackDuration;
            baseDealDamageTime = dealDamageTime;
        }

		humanoidRenderer = GetComponentInChildren<HumanoidRenderer>();
        baseObject.OnStateChange += OnStateChange;
        EquipWeapon(currentWeaponType, 60);
    }

    public bool CanPickUpWeapon()
    {
        return baseObject.GetObjectState() != ObjectState.Die
            || baseObject.GetObjectState() != ObjectState.Stun;
    }

    public void EquipWeapon(WeaponType weaponType, int _ammoCount) 
	{
        UnequipWeapon();
        currentEquippingWeapon = weaponManager.SpawnWeapon(weaponType);
        currentEquippingWeapon.InitWeapon(this, humanoidRenderer.rightHand, _ammoCount);

        attackRange = currentEquippingWeapon.range <= 0 ? baseRange : currentEquippingWeapon.range;
        attackDuration = currentEquippingWeapon.rateOfFire <= 0 ? baseAttackDuration : currentEquippingWeapon.rateOfFire;
        dealDamageTime = currentEquippingWeapon.dealDamageTime <= 0 ? baseDealDamageTime : currentEquippingWeapon.dealDamageTime;

        currentWeaponType = weaponType;
        OnWeaponEquipped(currentEquippingWeapon);

        humanoidRenderer.SetAnimatorOverride(weaponManager.GetWeaponOverrideController(weaponType));
        if (currentEquippingWeapon.GetWeaponRenderer() != null)
        {
            humanoidRenderer.AddRendererToList(currentEquippingWeapon.GetWeaponRenderer());
        }
    }

	public void UnequipWeapon()
	{
		if (currentEquippingWeapon != null) 
		{
			currentEquippingWeapon.UnEquipWeapon();
            if (currentEquippingWeapon.GetWeaponRenderer() != null)
            {
                humanoidRenderer.RemoveRendererFromList(currentEquippingWeapon.GetWeaponRenderer());
            }
        }
        currentEquippingWeapon = null;
        currentWeaponType = WeaponType.Unequipped;
	}

    public void DropWeaponOnGround()
    {
        if (currentEquippingWeapon != null && currentWeaponType != WeaponType.Unequipped)
        {
            weaponManager.SpawnPickUp(currentWeaponType, transform.position + transform.forward);
        }
        UnequipWeapon();
    }

	protected override void DealDamageToTarget (int _attackDamage = -1)
	{
		if (GetTargetObject() != null)
		{
            if (currentEquippingWeapon.CanPullTrigger())
            {
                currentEquippingWeapon.PullTrigger(GetTargetObject(), false);
            } else
            {
                EquipWeapon(WeaponType.Unequipped, -1);
            }
        }
	}

    private void OnStateChange(ObjectState s)
    {
        if (s == ObjectState.Die)
        {
            DropWeaponOnGround();
        }
    }

    public BaseWeapon GetCurrentWeapon() { return currentEquippingWeapon; }

    public WeaponType GetCurrentWeaponType() { return currentWeaponType; }

    public override ComponentType GetComponentType() { return ComponentType.WeaponUserComponent; }

}
