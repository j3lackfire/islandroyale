﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Rigidbody))]
public class MoveComponent : GameComponent {
    [Header("Movement")]
    [SerializeField]
    protected float moveSpeed;

    [SerializeField]
    private float positionCheckRate = 0.25f;
    [ReadOnly][SerializeField]
    private float positionCheckCountDown;
    [ReadOnly][SerializeField]
    private Vector3 previousPosition;
    [ReadOnly][SerializeField]
    private float previousYRotation;
    [SerializeField]
    private float minMovementThreadshold;
    [ReadOnly][SerializeField]
    private bool checkStuck = false;
    [ReadOnly][SerializeField]
    private bool isStuck = false;

    //component
    protected NavMeshAgent navMeshAgent;

    //cached variable for calculating.
    protected BaseObject targetObject = null;
    protected long targetID = -1;
    private float targetPosUpdateCountUp = 0f;
    protected Vector3 targetPosition = Vector3.zero;
    private bool shouldMoveSomewhere = false;
    private float angularSpeed = 1080;

    public override void InitComponent(BaseObject _baseObject, ObjectRenderer _objectRenderer, TeamManager _teamManager)
    {
        base.InitComponent(_baseObject, _objectRenderer, _teamManager);

        baseObject.EnableComponent += () => { navMeshAgent.enabled = true; };
        baseObject.DisableComponent += () => { navMeshAgent.enabled = false; };
        baseObject.OnPositionTargetted = SetTargetPos;
        baseObject.OnPositionSet += (Vector3 v) => { navMeshAgent.Warp(v); };

        navMeshAgent = GetComponent<NavMeshAgent>();
        shouldMoveSomewhere = false;
        navMeshAgent.speed = moveSpeed;
        navMeshAgent.acceleration = moveSpeed * 4;
        navMeshAgent.angularSpeed = angularSpeed;
        navMeshAgent.autoBraking = false;
        navMeshAgent.isStopped = false;

        minMovementThreadshold = moveSpeed / 4f;
        positionCheckCountDown = positionCheckRate;
        checkStuck = false;
        isStuck = false;
        previousPosition = transform.position;
        previousYRotation = transform.localEulerAngles.y;
    }

    public override bool ShouldActiveComponent()
    {
        return shouldMoveSomewhere;
    }

    public override bool ShouldStopComponent()
    {
        return IsObjectStuck() ||  IsTargetPositionReached();
    }

    public override void RegisterComponent<T>(T firstParameter, BoolDelegate activeCondition, BoolDelegate stoppingCondition, bool _isManagerCall)
    {
        base.RegisterComponent(firstParameter, activeCondition, stoppingCondition, _isManagerCall);

        if (typeof(T) == typeof(Vector3))
        {
            targetPosition = (Vector3)(object)firstParameter;
            SetTargetObject(null);
        } else if (typeof(T) == typeof(BaseObject))
        {
            SetTargetObject((BaseObject)(object)firstParameter);
            targetPosition = GetTargetObject().transform.position;
            targetPosUpdateCountUp = 0f;
        }

        shouldMoveSomewhere = true;
        navMeshAgent.SetDestination(targetPosition);
        if (_isManagerCall && ActiveCondition())
        {
            ForceStartComponent();
        }
    }

    public override void ComponentUpdate()
    {
        base.ComponentUpdate();
        if(GetTargetObject() != null && teamManager.CanSeeTarget(GetTargetObject()))
        {
            targetPosUpdateCountUp += Time.deltaTime;
            if (targetPosUpdateCountUp >= 1f)
            {
                navMeshAgent.SetDestination(GetTargetObject().transform.position);
            }
        }
        CheckStuck();
    }

    //external function, called by manager, to order the object to move somewhere
    //if the call is from manager/user or from the unit itself
    protected void SetTargetPos(Vector3 position, bool _isManagerCall)
    {
        RegisterComponent<Vector3>(position, null, null, _isManagerCall);
    }

    protected void SetTarget(BaseObject baseObject, bool _isManagerCall)
    {
        RegisterComponent<BaseObject>(baseObject, null, null, _isManagerCall);
    }

    private void SetTargetObject(BaseObject _target)
    {
        targetObject = _target;
        targetID = _target == null ? -1 : _target.GetID();
    }

    protected BaseObject GetTargetObject()
    {
        //if the object is already changed due to pooling, then return null.
        return (targetObject == null || IsTargetChanged()) ? null : targetObject;
    }

    protected bool IsTargetChanged() { return targetID != targetObject.GetID(); }

    public override void SetAsActiveComponent()
    {
        base.SetAsActiveComponent();
        navMeshAgent.isStopped = false;
        navMeshAgent.angularSpeed = angularSpeed;
        PlayAnimation();
    }

    private bool IsTargetPositionReached() { return (transform.position - targetPosition).magnitude <= 0.5f; }

    private void OnTargetPositionReached()
    {
        navMeshAgent.SetDestination(transform.position);
        OnComponentFinished();
		baseObject.SetIdle();
    }

    public override void OnComponentFinished()
    {
        base.OnComponentFinished();
        navMeshAgent.isStopped = true;
        navMeshAgent.angularSpeed = 0;
        shouldMoveSomewhere = false;
        checkStuck = false;
        isStuck = false;
    }

    public void StopMoving()
    {
        //send a signal and tell the object that it is now stuck. Will stop the move component completely
        isStuck = true;
    }

    private void CheckStuck()
    {
        positionCheckCountDown -= Time.deltaTime;
        if (positionCheckCountDown <= 0)
        {
            positionCheckCountDown = positionCheckRate;
            bool isStuckSinceLastFrame = ((transform.position - previousPosition).magnitude <= minMovementThreadshold 
                && (transform.localEulerAngles.y - previousYRotation <= 30f));
            if (isStuckSinceLastFrame)
            {
                if (!checkStuck)
                {
                    checkStuck = true;
                } else
                {
                    isStuck = true;
                }
            } else
            {
                checkStuck = false;
                isStuck = false;
                previousPosition = transform.position;
                previousYRotation = transform.localEulerAngles.y;
            }
        }
    }

    public bool IsObjectStuck() { return isStuck; }

    public override ComponentType GetComponentType() { return ComponentType.MoveComponent; }

#if UNITY_EDITOR
    public bool EditorShouldMoveSomeWhere() { return shouldMoveSomewhere; }

    public Vector3 EditorTargetPosition() { return targetPosition; }
#endif
}
