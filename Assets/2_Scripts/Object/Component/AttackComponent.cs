﻿using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class AttackComponent : GameComponent
{
    [Header("Attack DATA")]
    public bool attackUnit;
    public bool attackBuilding;

    //This is the length of the attack animation
    public float attackDuration;
    public float dealDamageTime;

    public int baseDamage;
    [ReadOnly][SerializeField]
    protected int damage;

    public float attackRange;
    public float agressiveRange;

    [Header("Cached values and stuff")]
    //absolutely no reason to touch these two variables. use my getter function.
    [ReadOnly][SerializeField]
    private BaseObject targetObject;
    [ReadOnly][SerializeField]
    private long targetID;

    //cache value for calculation and stuffs
    [ReadOnly][SerializeField]
    protected bool isDamageDeal;
    [ReadOnly][SerializeField]
    protected float attackCountUp;

    protected float requestTargetCountUp = 0f;
    //request new target every 0.5 seconds if can't found any
    protected float requestTargetFrequency = 0.5f;

    protected float followTargetCountUp = 0f;
    [SerializeField] //will keep run after the target 1 seconds after lost LOS
    protected float followTargetAfterLOSTime = 10f;

    [ReadOnly][SerializeField]
    protected bool canSwitchTarget;

    protected MoveComponent moveComponent;

    public override void InitComponent(BaseObject _baseObject, ObjectRenderer _objectRenderer, TeamManager _teamManager)
    {
        base.InitComponent(_baseObject, _objectRenderer, _teamManager);

        requestTargetCountUp = requestTargetFrequency * 0.8f;
        attackCountUp = 0;
        damage = baseDamage;
        isDamageDeal = false;
        canSwitchTarget = true;

        agressiveRange = agressiveRange >= baseObject.sight ? agressiveRange : baseObject.sight;

        moveComponent = GetComponent<MoveComponent>();

        baseObject.OnEnemyObjectTargetted = SetTarget;

        baseObject.OnStateChange += (ObjectState t) =>
        {
            if (t == ObjectState.Idle)
            {
                canSwitchTarget = true;
            }
        };

        baseObject.OnDamageRecieve += (int damage, AttackComponent attacker) =>
        {
            if (baseObject.GetObjectState() == ObjectState.Idle)
            {
                SetTarget(attacker.baseObject, false);
            }
        };
    }

    //target is not null, can be attacked and is within attack range.
    public override bool ShouldActiveComponent()
    {
        if (!CanAttackTarget())
        {
            requestTargetCountUp += Time.deltaTime;
            if (canSwitchTarget && requestTargetCountUp >= requestTargetFrequency)
            {
                requestTargetCountUp = 0f;
                BaseObject requestedTarget = teamManager.RequestTarget(this);
                if (requestedTarget == null)
                {
                    return false;
                }
                if (IsTargetInSight(requestedTarget))
                {
                    SetTarget(requestedTarget, false);
                }
            } 
            return false;
        } else
        {
            return true;
        }
    }

    //maybe I should do something about this ????
    public override void ForceStopComponent()
    {
        base.ForceStopComponent();
        requestTargetCountUp = requestTargetFrequency * 0.8f;
    }

    private bool CanAttackTarget()
    {
        return ((GetTargetObject() != null)
            && GetTargetObject().CanBeAttacked()
            && IsTargetInRange()
            //&& LOSHelper.HasLOS(transform.position, GetTargetObject().transform.position));
            && LOSHelper.CanSeeTarget(GetBaseObject(), GetTargetObject().transform.position));
    }

    public override void RegisterComponent<T>(T firstParameter, BoolDelegate activeCondition, BoolDelegate stoppingCondition, bool _isManagerCall)
    {
        base.RegisterComponent<T>(firstParameter, activeCondition, stoppingCondition, _isManagerCall);
        canSwitchTarget = !_isManagerCall;
        SetTargetObject(firstParameter as BaseObject);

        if (_isManagerCall && ActiveCondition())
        {
            ForceStartComponent();
        }
    }

    protected void SetTarget(BaseObject _target, bool _isManagerCall)
    {
        RegisterComponent<BaseObject>(_target, null, null, _isManagerCall);
        /*if target is out of range and the object can MOVE
        move to the target game object, with the exit condition of the MOVE component
        is if the target is in range*/
        if (!CanAttackTarget() && moveComponent != null)
        {
			BoolDelegate movementExitCondition = () => 
            {
                if (moveComponent.ShouldStopComponent() ||  ActiveCondition() || GetTargetObject() == null || !targetObject.CanBeAttacked())
                {
                    return true;
                } else
                {
                    if (!teamManager.CanSeeTarget(targetObject))
                    {
                        //complicated calculation here
                        followTargetCountUp += Time.deltaTime;
                        return followTargetCountUp >= followTargetAfterLOSTime;
                    } else
                    {
                        followTargetCountUp = 0f;
                        return false;
                    }
                }
                
			};
			baseObject.RegisterComponent(typeof(MoveComponent), _target, null, movementExitCondition, _isManagerCall);
			//moveComponent.RegisterComponent(_target, null, movementExitCondition, _isManagerCall);
        }
        //Debug.Log("Distance to target " + (baseObject.transform.position - GetTargetObject().gameObject.transform.position).magnitude);
    }

    //equivalent to the update function
    public override void ComponentUpdate()
    {
        //it should be attack count up, shouldn't it ? LOL
        attackCountUp += Time.deltaTime;
        if (!isDamageDeal)
        {
            if (GetTargetObject() == null || !GetTargetObject().CanBeAttacked())
            {
                FinishAttackTarget();
            } else
            {
                if (attackCountUp >= dealDamageTime)
                {
                    DealDamageToTarget();
                    isDamageDeal = true;
                }
            }
        }
        else
        {
            if (attackCountUp >= attackDuration)
            {
                FinishAttackTarget();
            }
        }
    }

    public override void SetAsActiveComponent()
    {
        base.SetAsActiveComponent();
        StartAttackTarget();
    }

    //start attack target.
    public virtual void StartAttackTarget()
    {
        if (GetTargetObject() != null)
        {
            PlayAnimation();
            baseObject.transform.LookAt(GetTargetObject().transform.position);
            isDamageDeal = false;
            attackCountUp = 0;
        } else
        {
            baseObject.SetIdle();
        }
    }

    //in case deal special damage that is not normal attack damage
    protected virtual void DealDamageToTarget(int _attackDamage = -1)
    {
        if (GetTargetObject() != null)
        {
            targetObject.OnDamageRecieve(_attackDamage == -1 ? damage : _attackDamage, this);
        }
    }

    protected void FinishAttackTarget()
    {
        isDamageDeal = false;
        if (ActiveCondition()) 
        //if (GetTargetObject() != null && GetTargetObject().CanBeAttacked())
        {
            StartAttackTarget();
        }
        else
        {
            if (moveComponent != null && GetTargetObject() != null && GetTargetObject().CanBeAttacked() && !IsTargetInRange() && isExplicitOrder)
            {
                //move to the target
                SetTarget(GetTargetObject(), isExplicitOrder);
            }
            else
            {
                BaseObject requestedTarget = teamManager.RequestTarget(this);
                SetTargetObject(requestedTarget);
                if (!ActiveCondition())
                //if (requestedTarget == null || !IsTargetInRange() || !requestedTarget.CanBeAttacked())
                {
                    OnComponentFinished();
                    baseObject.SetIdle();
                }
                else
                {
                    SetTarget(requestedTarget, isExplicitOrder);
                    StartAttackTarget();
                }
            }
        }
    }

	public override bool ShouldStopComponent() {
		if (isDamageDeal) 
		{
			return false;
		}
		if (GetTargetObject () == null || !GetTargetObject ().CanBeAttacked () || !IsTargetInSight()) {
			return true;
		} else {
			return false;
		}
	}

    private void SetTargetObject(BaseObject _target)
    {
        targetObject = _target;
        targetID = _target == null ? -1 : _target.GetID();
    }

    //if the object is already changed due to pooling, then return null.
    //public for editor testing
    public BaseObject GetTargetObject()
    {
        return (targetObject == null || IsTargetChanged()) ? null : targetObject;
    }

    protected bool IsTargetChanged() { return targetID != targetObject.GetID(); }

    //default variable, check the current target,
    //not, boolean check for a target
    protected bool IsTargetInRange(BaseObject _target = null)
    {
        if (_target != null)
        {
            return (transform.position - _target.transform.position).magnitude <= attackRange;
        } else
        {
            return (transform.position - GetTargetObject().transform.position).magnitude <= attackRange;
        }
    }

    protected bool IsTargetInSight(BaseObject _target = null)
    {
        if (_target != null)
        {
            return (transform.position - _target.transform.position).magnitude <= agressiveRange;
        } else
        {
            return (transform.position - GetTargetObject().transform.position).magnitude <= agressiveRange;
        }
    }

    public override void OnComponentFinished()
    {
        base.OnComponentFinished();
    }

    public override ComponentType GetComponentType() { return ComponentType.AttackComponent; }
}

