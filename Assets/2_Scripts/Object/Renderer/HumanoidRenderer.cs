﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanoidRenderer : ObjectRenderer
{
    public Transform leftHand, rightHand;
    public AnimatorWrapper animatorWrapper;

    public override void InitRenderer(BaseObject _parentObject)
    {
        base.InitRenderer(_parentObject);

        animatorWrapper = new AnimatorWrapper(GetComponent<Animator>());

        //only aplicable for the simple fantasy pack :/
        //rightHand = transform.Find("Hips_jnt/Spine_jnt/Spine_jnt 1/Chest_jnt/Shoulder_Right_jnt/Arm_Right_jnt/Forearm_Right_jnt/Hand_Right_jnt");
        //leftHand = transform.Find("Hips_jnt/Spine_jnt/Spine_jnt 1/Chest_jnt/Shoulder_Left_jnt/Arm_Left_jnt/Forearm_Left_jnt/Hand_Left_jnt");

        rightHand = transform.Find("Root_jnt/Hips_jnt/Body_jnt/Spine_jnt/UpperArm_Right_jnt/LowerArm_Right_jnt/Hand_Right_jnt");
        leftHand = transform.Find("Root_jnt/Hips_jnt/Body_jnt/Spine_jnt/UpperArm_Left_jnt/LowerArm_Left_jnt/Hand_Left_jnt");

        baseObject.OnStateChange += OnStateChange;
    }

    public override void DoUpdateRenderer()
    {
        animatorWrapper.DoUpdate();
        base.DoUpdateRenderer();
    }

    private void setHands() {}

    private void OnStateChange(ObjectState state)
    {
        switch (state)
        {
            case (ObjectState.Invalid):
                Debug.Log("<color=red> Object State invalid, wtf ???!!!</color>");
                Debug.Break();
                break;
            case ObjectState.Idle:
                animatorWrapper.AddTriggerToQueue("EnterIdle");
                break;
            case ObjectState.Component:
                //do nothing, since this case will be covered in another function
                break;
            case ObjectState.Stun:
                //stun ??
                break;
            case ObjectState.Die:
                animatorWrapper.AddTriggerToQueue("EnterDeath");
                //die animation
                break;
        }
    }

    private void OnComponentActive(Type componentType) {}

    private bool IsSameOrSubClass(Type compareClass, Type baseClass)
    {
        return compareClass.IsSubclassOf(baseClass)
       || compareClass == baseClass;
    }

    public override void PlayAnimation(Type t)
    {
        base.PlayAnimation(t);
        if (IsSameOrSubClass(t, typeof(MoveComponent)))
        {
            animatorWrapper.AddTriggerToQueue("EnterRun");
        }
        else if (IsSameOrSubClass(t, typeof(AttackComponent)))
        {
            animatorWrapper.AddTriggerToQueue("EnterAttack");
        }
        else
        {
            //enter idle state
        }

    }

    public void SetAnimatorOverride(AnimatorOverrideController overrideController)
    {
        animatorWrapper.SetOverideAnimator(overrideController);
    }
}
