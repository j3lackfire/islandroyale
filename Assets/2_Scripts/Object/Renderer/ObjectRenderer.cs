﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRenderer : MonoBehaviour {

    protected BaseObject baseObject;
    protected ProgressBar healthBar;
    [ReadOnly][SerializeField]
    protected List<Renderer> myRenderers;

    [ReadOnly][SerializeField]
    private bool isRendererShow = false;

    private bool shoudHideRenderer;

    public virtual void InitRenderer(BaseObject _parentObject)
    {
        baseObject = _parentObject;

        myRenderers = new List<Renderer>();
        myRenderers.AddRange(baseObject.GetComponentsInChildren<Renderer>());
        healthBar = baseObject.GetComponentInChildren<ProgressBar>();
        healthBar.InitBar(this);
        healthBar.UpdateBar((float)baseObject.GetHealth() / (float)baseObject.GetMaxHealth());

        baseObject.OnHealthChange += OnHealthChanged;
        baseObject.ShowRenderer = ShowRenderer;
        baseObject.HideRenderer = SetHideRenderer;
        isRendererShow = false;
        ShowRenderer();
    }

    public virtual void DoUpdateRenderer()
    {
        if (shoudHideRenderer && isRendererShow)
        {
            HideRenderer();
        }
    }

    public void AddRendererToList(Renderer r)
    {
        myRenderers.Add(r);
    }

    public void RemoveRendererFromList(Renderer r)
    {
        myRenderers.Remove(r);
    }

    public virtual void PlayAnimation(Type t) { }

    protected virtual void OnHealthChanged(int newHealth)
    {
        healthBar.UpdateBar((float)baseObject.GetHealth() / (float)baseObject.GetMaxHealth());
    }

    private void ShowRenderer()
    {
        shoudHideRenderer = false;
        if (!isRendererShow)
        {
            isRendererShow = true;
            shoudHideRenderer = false;
            foreach (Renderer r in myRenderers)
            {
                r.enabled = true;
            }

            healthBar.ShowBar();
        }
    }

    private void SetHideRenderer()
    {
        shoudHideRenderer = true;
    }

    private void HideRenderer()
    {
        isRendererShow = false;
        foreach (Renderer r in myRenderers)
        {
            r.enabled = false;
        }
        healthBar.HideBar();
    }
}
