﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Wrapper for the animator, to avoid case where the animator receive
 * Two order and mess up completely.
 */

[System.Serializable]
public class AnimatorWrapper
{
    [ReadOnly][SerializeField]
    private Animator thisAnimator;

    [ReadOnly][SerializeField]
    private List<string> triggerList = new List<string>();


    public AnimatorWrapper(Animator targetAnimator)
    {
        thisAnimator = targetAnimator;
    }

    public void DoUpdate()
    {
        if (triggerList.Count > 0)
        {
            if (!thisAnimator.IsInTransition(0))
            {
                thisAnimator.SetTrigger(triggerList[0]);
                //pastTriggerList.Add(new AnimationInfo(triggerList[0], Time.timeSinceLevelLoad));
                triggerList.RemoveAt(0);
            }
        }
    }

    public void AddTriggerToQueue(string triggerName)
    {
        triggerList.Add(triggerName);
    }

    public void SetOverideAnimator(AnimatorOverrideController newAnimator)
    {
        thisAnimator.runtimeAnimatorController = newAnimator;
    }

    //[ReadOnly]
    //[SerializeField]
    //private List<AnimationInfo> pastTriggerList = new List<AnimationInfo>();

    //[System.Serializable]
    //public struct AnimationInfo
    //{
    //    public string triggerName;
    //    public float timeActive;

    //    public AnimationInfo(string name, float time)
    //    {
    //        triggerName = name;
    //        timeActive = time;
    //    }
    //}
}
