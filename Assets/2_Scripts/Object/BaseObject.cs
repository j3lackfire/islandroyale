﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[SelectionBase]
public class BaseObject : PooledObject {
    public static readonly string objectTag = "BaseObject";
    //COMPONENTS
    [Header("Components and Managers")]
    protected TeamManager teamManager;

    protected ObjectRenderer objectRenderer;
    protected Collider objectCollider;
    protected NavMeshObstacle navMeshObstacle;

    [ReadOnly][SerializeField]
    private List<GameComponent> componentList;
    [ReadOnly][SerializeField]
    private int activeComponentIndex;

    private List<BackgroundComponent> backGroundComponents;

    [ReadOnly][SerializeField]
    private List<ComponentType> componentTypeList;

    #region DELEGATES
    //OVERRIDE ONLY
    public VoidDelegate ShowRenderer;
    public VoidDelegate HideRenderer;

    //ADDITIVE
    //disable component while the object is dead
    public VoidDelegate DisableComponent;
    //enable component when the object is reinit again.
    public VoidDelegate EnableComponent = () => { };

    public VoidDelegateInt OnHealthChange;

    public VoidDelegateDamageReceive OnDamageRecieve;

    public VoidDelegateStateChange OnStateChange = (ObjectState s) => { };

    public delegate void ComponentDelegate(Type t);
    public ComponentDelegate OnComponentActive = (Type t) => { };
    public ComponentDelegate OnComponentFinished = (Type t) => { };

    public delegate void TargetPositionDelegate (Vector3 v, bool canBeInterupted);
    public TargetPositionDelegate OnPositionTargetted;

    public delegate void TargetObjectDelegate(BaseObject b, bool _isManagerCall);
    public TargetObjectDelegate OnEnemyObjectTargetted;
    public TargetObjectDelegate OnFriendlyObjectTargetted;

    public VoidDelegateVector3 OnPositionSet = (Vector3 v) => { };
#endregion

    [Header("Variables")]
    //calculated value - code calculate value
    public int baseHealth;
    [NonSerialized]
    protected int maxHealth;
    [ReadOnly][SerializeField]
    protected int health; //current health

    public float respawnTime;
    
    //public variables (for other unit to check)
    [SerializeField] //set in the editor, there is a getter function right below
    private ObjectType objectType;
    [SerializeField]
    protected float objectDeathTime = 10f;
    [ReadOnly][SerializeField]
    protected ObjectState objectState;
    [ReadOnly][SerializeField]
    protected bool isTemporaryInvurable;

    //Cache variables, mostly for flagging and checking 
    protected float idleCountDown; //The amount of frame to check for new target when idle
    [ReadOnly][SerializeField]
    protected float deathCountDown; //cache value, for death and calculation

    [Header("Visions")]
    public float fieldOfView = 102f;
    public float sight = 20f;
    [Tooltip("The view radius from behind the object")]
    public float backSight = 7f;
    public bool requireLOS = true;
    public FogMaskType fogMaskType;
    [SerializeField]
    protected float deadProvideSightTime = 2f; //time unit still gives sight and view after dead.

    [ReadOnly][SerializeField]
    protected bool canProvideSight = true;

    //the int _team can also be get from the _teamManager to make it less parameter
    public virtual void Init(TeamManager _teamManager)
    {
        //for testing and easier debuging.
        gameObject.name = GetObjectType().ToString() + "_" + GetID().ToString();
        //caching and stuff
        gameObject.tag = objectTag;
        teamManager = _teamManager;
        objectCollider = GetComponent<Collider>();
        navMeshObstacle = GetComponent<NavMeshObstacle>();

        OnFirstInit();
        SetObjectVariables();
        PrepareComponent();
        SetIdle();
    }

    protected override void OnFirstInit()
    {
        base.OnFirstInit();
        if (objectType == ObjectType.Invalid)
        {
            Debug.Log("Invalid object type !!! " + gameObject.name, this.gameObject);
            Debug.Break();
        }
        //cache the managers
    }
    
    public virtual void SetObjectVariables()
    {
        componentTypeList = new List<ComponentType>();
        maxHealth = baseHealth;
        health = maxHealth;
        isTemporaryInvurable = false;
        deathCountDown = 0f;
        canProvideSight = true;
    }

    //prepare the child components, like animator, renderer and stuff
    protected virtual void PrepareComponent()
    {
        //by default, all base object will be in the BaseObject layer, unless
        //another object override it below, then well...
        gameObject.layer = LayerMask.NameToLayer("BaseObject");
        EnableComponent();
        #region DELEGATES SETUP
        //reset all the delegate
        EnableComponent = () =>
        {
            objectCollider.enabled = true;
            if (navMeshObstacle != null)
            {
                navMeshObstacle.enabled = true;
            }
        };
        DisableComponent = () =>
        {
            objectCollider.enabled = false;
            if (navMeshObstacle != null)
            {
                navMeshObstacle.enabled = false;
            }
        };
        OnStateChange = (ObjectState s) => { };
        OnComponentActive = (Type t) => { };
        OnComponentFinished = (Type t) => { };
        OnPositionTargetted = (Vector3 v, bool b) => { };
        OnEnemyObjectTargetted = (BaseObject bo, bool b) => { };
        OnFriendlyObjectTargetted = (BaseObject bo, bool b) => { };
        OnPositionSet = (Vector3 v) => { };
        ShowRenderer = () => { };
        HideRenderer = () => { };
        OnHealthChange = SetHealth;

        OnDamageRecieve = ReceiveDamage;

        objectRenderer = GetComponentInChildren<ObjectRenderer>();
        objectRenderer.InitRenderer(this);

        //get all component, add it to a list, and sort it
        componentList = new List<GameComponent>();
        componentList.AddRange(GetComponents<GameComponent>());
        componentList.Sort((x, y) => x.GetPriority().CompareTo(y.GetPriority()));
        foreach (GameComponent g in componentList)
        {
            g.InitComponent(this, objectRenderer, teamManager);
        }
        activeComponentIndex = -1;

        //background component
        backGroundComponents = new List<BackgroundComponent>();
        backGroundComponents.AddRange(GetComponents<BackgroundComponent>());
        foreach (BackgroundComponent b in backGroundComponents)
        {
            b.InitComponent(this, teamManager);
        }
        #endregion
        EnableComponent();
    }

    public void AddComponentTypeToList(ComponentType _type) { componentTypeList.Add(_type); }

    public bool HasComponent(ComponentType _type) { return componentTypeList.Contains(_type); }

    //Update is called every frame by this object manager.
    public virtual void DoUpdate()
    {
        switch (objectState)
        {
            case (ObjectState.Invalid):
                Debug.Log("<color=red> Object State invalid, wtf ???!!!</color>");
                Debug.Break();
                break;
            case ObjectState.Idle:
                ObjectIdle();
                break;
            case ObjectState.Component:
                ComponentUpdate();
                break;
            case ObjectState.Stun:
                ObjectStun();
                break;
            case ObjectState.Die:
                WhileObjectDie();
                break;
        }
        objectRenderer.DoUpdateRenderer();
        foreach(BackgroundComponent bc in backGroundComponents)
        {
            bc.ComponentUpdate();
        }
    }

    //just set the state of this to idle
    public void SetIdle()
    {
        ForceStopComponentExcept(null);
        activeComponentIndex = -1;
        SetState(ObjectState.Idle);
    }

    //What should this object do when a state is changed ????
    protected virtual void SetState(ObjectState state)
    {
        objectState = state;
        // the delegate, other component can subcribe to this function.
        OnStateChange(state);
    }

    protected virtual void ObjectIdle() { CheckForActiveComponent(); }

    private void CheckForActiveComponent()
    {
        for (int i = 0; i < componentList.Count; i++)
        {
            //if component check return true, it means the component should be
            //active now, and the object change state to whaever component it has
			if (componentList[i].ActiveCondition())
            {
                ActiveComponent(i);
                break;
            }
        }
    }

	public void RegisterComponent<T>(Type componentType, T firstParameter, BoolDelegate activeCondition, BoolDelegate stoppingCondition, bool _isManagerCall) 
	{
		for(int i = 0; i < componentList.Count; i++) 
		{
			if (componentList[i].GetType() == componentType) 
			{
				componentList[i].RegisterComponent<T>(firstParameter, activeCondition, stoppingCondition, _isManagerCall);
				break;
			}
		}
	}

    private void ActiveComponent(int index)
    {
        activeComponentIndex = index;
        SetState(ObjectState.Component);
        componentList[index].SetAsActiveComponent();
    }

    private void ComponentUpdate()
    {
        //if the current action can not be interupted, then let it be
        GameComponent currentComponent = componentList[activeComponentIndex];
        currentComponent.ComponentUpdate();
        if (currentComponent.StoppingCondition())
        {
            currentComponent.OnComponentFinished();
            SetIdle();
        }
        if (currentComponent.CanBeInterupted())
        {
            for (int i = 0; i < componentList.Count; i++)
            {
                if (!componentList[i].CanBeInterupted() && componentList[i].ActiveCondition())
                {
                    ActiveComponent(i);
                    //componentList[i].ComponentUpdate();
                    break;
                }
            }
        }
    }

    public GameComponent GetCurrentActiveComponent() { return activeComponentIndex == -1 ? null : componentList[activeComponentIndex]; }

    //force start a component, usually by user input, will stop all other component but this.
    public void ForceStopComponentExcept(Type t) {
        if (activeComponentIndex != -1)
        {
            if (t == null)
            {
                componentList[activeComponentIndex].ForceStopComponent();
            } else
            {
                if (componentList[activeComponentIndex].GetType() != t)
                {
                    componentList[activeComponentIndex].ForceStopComponent();
                    bool isComponentFound = false;
                    for (int i = 0; i < componentList.Count; i++)
                    {
                        if (componentList[i].GetType() == t)
                        {
                            isComponentFound = true;
                            ActiveComponent(i);
                        }
                    }
                    if (!isComponentFound)
                    {
                        Debug.Log("<color=red> Super error here, can't active component </color>");
                        Debug.Break();
                    }
                }
            }
        }
	}

    protected virtual void ObjectStun() { }

    protected virtual void WhileObjectDie()
    {
        deathCountDown += Time.deltaTime;
        if (deathCountDown >= deadProvideSightTime && canProvideSight)
        {
            canProvideSight = false;
            teamManager.OnObjectStopProvidingSight(this);
        }

        if (deathCountDown >= objectDeathTime)
        {
            deathCountDown = -100f;
            KillObject();
        }
    }

    //Passing the attacker in to check detail
    protected virtual void ReceiveDamage(int damage, AttackComponent attacker = null)
    {
        //Complex calculation here later if I want the game to get complex
        ReduceHealth(damage);
    }

    protected virtual void ReduceHealth(int damage)
    {
        OnHealthChange(health - damage);
        if (health <= 0)
        {
            OnObjectDie();
        }
    }

	public int GetHealth() { return health; }

	public int GetMaxHealth() { return maxHealth; }

	public void SetHealth(int value) { health = Mathf.Min(value, maxHealth); }

    public virtual void OnObjectDie()
    {
        //Debug.Log("<color=#abcabc> I am die </color>" + gameObject.name);
        SetState(ObjectState.Die);
        deathCountDown = 0f;
        isTemporaryInvurable = true;
        ForceStopComponentExcept(null);
        DisableComponent();
    }

    protected virtual void DeadEffect() {}

    protected virtual void KillObject()
    {
        teamManager.RemoveObject(this);
        ReturnToPool();
        ShowRenderer();
    }

    public virtual void ForceMoveObjectTo(Vector3 position)
    {
        SetState(ObjectState.Idle);
        transform.position = position;
        OnPositionSet(position);
    }

    public virtual void ForceMoveObjectBy(Vector3 deltaPosition)
    {
        Vector3 newPos = transform.position + deltaPosition;
        ForceMoveObjectTo(newPos);
    }

    public ObjectType GetObjectType() { return objectType; }

    public int GetObjectTeam() { return teamManager.GetTeam(); }

    public ObjectState GetObjectState() { return objectState; }

    public virtual bool CanBeAttacked() { return isTemporaryInvurable ? false : GetObjectState() != ObjectState.Die; }

    public bool CanProvideSight() { return GetObjectState() != ObjectState.Die ? true : canProvideSight; }
}
