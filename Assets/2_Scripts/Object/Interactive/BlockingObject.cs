﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class BlockingObject : MonoBehaviour {

    [SerializeField][ReadOnly]
    private Collider myCollider;

    public void InitBlockingObject(bool _isOn)
    {
        Collider c = GetCollider();
        c.enabled = _isOn;
    }

    private Collider GetCollider()
    {
        if (myCollider == null)
        {
            myCollider = GetComponent<Collider>();
        }
        return myCollider;
    }

    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "BaseObject")
        {
            BaseObject baseObject = c.GetComponent<BaseObject>();
            if (baseObject.HasComponent(ComponentType.MoveComponent))
            {
                //do something????
                //MoveComponent moveComponent = c.GetComponent<MoveComponent>();
                //moveComponent.StopMoving();
            }
        }
    }
}
