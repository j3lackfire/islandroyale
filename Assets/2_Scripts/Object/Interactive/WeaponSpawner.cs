﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSpawner : ObjectSpawner {
    private WeaponManager weaponManager;

    [SerializeField]
    protected List<WeaponSpawnerSetting> spawnedWeapons;
    private Renderer weaponPreview;

    protected override void Start()
    {
        base.Start();
        weaponPreview = transform.GetComponentInDirectChildren<Renderer>();
        weaponManager = Director.instance.weaponManager;
        SpawnWeapon();
    }

    private void SpawnWeapon()
    {
        if (spawnedWeapons.Count == 0)
        {
            //don't do anything
            Debug.Log("<color=red>ERROR, there is no spawned weapon</color>", this.gameObject);
        } else
        {
            int totalSpawnRate = 0;
            for (int i = 0; i < spawnedWeapons.Count; i ++)
            {
                totalSpawnRate += spawnedWeapons[i].spawnWeight;
            }
            int randomRate = UnityEngine.Random.Range(0, totalSpawnRate);
            int currentRate = 0;
            for (int i = 0; i < spawnedWeapons.Count; i ++)
            {
                currentRate += spawnedWeapons[i].spawnWeight;
                if (randomRate < currentRate)
                {
                    weaponManager.SpawnPickUp(spawnedWeapons[i].weaponType, transform.position, transform.localEulerAngles);
                    break;
                }
            }
        }
        Destroy(this.gameObject);
    }
}

[System.Serializable]
public struct WeaponSpawnerSetting
{
    public WeaponType weaponType;
    public int spawnWeight;
}