﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
[RequireComponent(typeof(Collider))]
public class InteractableObject : PooledObject {
    [ReadOnly][SerializeField]
    protected InteractiveComponent interactiveComponent;

    private float checkForInactiveColliderCountDown = 0.3f;

    [SerializeField]
    private string actionName;
    [SerializeField]
    private string objectName;

    protected virtual void Awake()
    {

    }

    protected virtual void OnEnable()
    {
        interactiveComponent = null;
    }

    protected virtual void Update()
    {
        checkForInactiveColliderCountDown -= Time.deltaTime;
        if (checkForInactiveColliderCountDown <= 0f)
        {
            checkForInactiveColliderCountDown = 0.3f;
            if (interactiveComponent != null && !interactiveComponent.gameObject.activeInHierarchy)
            {
                OnExitWith(interactiveComponent);
            }
        }
    }

    //for example, door is open door, and, well, whatever ....
    public virtual void InteractiveFunction(InteractiveComponent user) {}

    protected virtual void OnTriggerEnter(Collider c)
    {
        if (interactiveComponent == null && c.tag == BaseObject.objectTag)
        {
            InteractiveComponent ic = c.GetComponent<InteractiveComponent>();
            if (ic != null)
            {
                OnEnterWith(ic);
            }
        }
    }

    protected virtual void OnTriggerExit(Collider c)
    {
        if (interactiveComponent != null && c.gameObject == interactiveComponent.gameObject)
        {
            OnExitWith(interactiveComponent);
        }
    }

    protected void OnEnterWith(InteractiveComponent ic)
    {
        interactiveComponent = ic;
        interactiveComponent.OnEnter(this);
    }

    protected void OnExitWith(InteractiveComponent ic)
    {
        interactiveComponent.OnExit(this);
        interactiveComponent = null;
    }

    public virtual string GetActionName() { return actionName; }

    public virtual string GetObjectName() { return objectName; }
}
