﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshObstacle))]
public class BaseDoor : InteractableObject {

    private GameObject theDoor;
    private NavMeshObstacle doorObstacle;

    [SerializeField]
    private Vector3 openPosition;
    [SerializeField]
     private Vector3 openRotation;

    [SerializeField]
    private Vector3 closedPosition;
    [SerializeField]
    private Vector3 closedRotation;

    [SerializeField]
    private float animationTime = 0.5f;
    private float countDown = 0f;

    [SerializeField]
    private bool isDoorOpen = false;

    //tweening sequences
    private Sequence mySequence;

    protected override void Awake()
    {
        base.Awake();
        theDoor = transform.Find("Door").gameObject;
        doorObstacle = GetComponent<NavMeshObstacle>();
        mySequence = DOTween.Sequence();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        countDown = 0f;
        if (isDoorOpen)
        {
            OpenDoor();
        } else
        {
            CloseDoor();
        }
    }

    public bool IsDoorOpen() { return isDoorOpen; }

    public override void InteractiveFunction(InteractiveComponent user)
    {
        base.InteractiveFunction(user);
        if (isDoorOpen) {
            CloseDoor();
        } else
        {
            OpenDoor();
        }
    }

    public override string GetActionName()
    {
        return isDoorOpen ? "CLOSE" : "OPEN";
    }

    private void OpenDoor()
    {
        isDoorOpen = true;
        theDoor.transform.DOLocalRotate(openRotation, animationTime);
        theDoor.transform.DOLocalMove(openPosition, animationTime);
        doorObstacle.enabled = false;
    }

    private void CloseDoor()
    {
        isDoorOpen = false;
        theDoor.transform.DOLocalRotate(closedRotation, animationTime);
        theDoor.transform.DOLocalMove(closedPosition, animationTime);
        doorObstacle.enabled = true;
    }

#if UNITY_EDITOR
    public void SaveOpenDoorPosition()
    {
        Transform door = transform.Find("Door");
        openPosition = door.localPosition;
        openRotation = door.localEulerAngles;
    }

    public void SaveCloseDoorPosition()
    {
        Transform door = transform.Find("Door");
        closedPosition = door.localPosition;
        closedRotation = door.localEulerAngles;
    }
#endif
}

