﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
using System.Reflection;
#endif

public class Director : MonoBehaviour {

    /*
     * Singleton. ONLY use singleton for this.  
     * For everything else, access though THE director 
     */
    private static Director _instance;
    public static Director instance
    {
        get { return _instance; }
        private set { _instance = value; }
    }

    [HideInInspector]
    private List<BaseManager> managersList;

    //input
    public InputManager inputManager;
    public UIManager uiManager;
    public HighLightManager highLightManager;

    public WeaponManager weaponManager;
    public BulletManager bulletManager;

    public PlayerManager playerManager;
    public AIManager aiManager;

    public CameraManager cameraManager;
	public FogOfWarManager fogOfWarmanager;


    public void Awake()
    {
        #region singleton check
        if (instance == null)
        {
            instance = FindObjectOfType<Director>();
        }
        else
        {
            if (instance.gameObject == null)
            {
                instance = FindObjectOfType<Director>();
            }
            else
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion

        InitManager();
        PrefabsManager.PreparePrefabsManager();
        LOSHelper.PrepareLOSHelper();
    }

    public void Update()
    {
        for (int i = 0; i < managersList.Count; i++)
        {
            managersList[i].DoUpdate();
        }
    }

    private void InitManager()
    {
        managersList = new List<BaseManager>();

        //-------------- IMPORTANT STUFF HERE -------------
        //the order of prepare is also the order to init of the manager
        inputManager = PrepareManager(inputManager);
        highLightManager = PrepareManager(highLightManager);

        weaponManager = PrepareManager(weaponManager);
        bulletManager = PrepareManager(bulletManager);

        playerManager = PrepareManager(playerManager);
        aiManager = PrepareManager(aiManager);

        cameraManager = PrepareManager(cameraManager);
		fogOfWarmanager = PrepareManager(fogOfWarmanager);

        uiManager = PrepareManager(uiManager);
        //-------------- END OF IMPORTANT STUFF -------------

        for (int i = 0; i < managersList.Count; i++)
        {
            managersList[i].Init();
        }
    }

    //super clever prepare manager function I think of xD
    private T PrepareManager<T>(T tManager) where T : BaseManager
    {
        T tempT = (T)FindObjectOfType(typeof(T));
        if (tManager != null)
        {
            Destroy(tManager.gameObject);
        }
        managersList.Add(tempT);
        return tempT;
    }

    //----- Editor test function-------
#if UNITY_EDITOR
    //public GameObject myObject;
    //public GameObject test_1;

    public void EditorTest()
    {
        //Debug.Log(myObject.transform.forward);
        //Vector3 direction = test_1.transform.position - myObject.transform.position;
        //Debug.Log(Vector3.Angle(myObject.transform.forward, direction));
        //Debug.Log(Vector3.Angle(direction, myObject.transform.forward));
    }

    //press ctrl + shift + C to clear debug console.
    [MenuItem("Tools/Clear Console %#c")] // CMD + SHIFT + C
    public static void ClearConsole()
    {
        var assembly = Assembly.GetAssembly(typeof(SceneView));
        var type = assembly.GetType("UnityEditorInternal.LogEntries");
        var method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }

#endif
}

public enum GameState
{
    Invalid = -1,
    Editor = 0,
    Play
}