﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamManager : BaseManager {

	//cached and managers stuff
	private List<TeamManager> enemyTeams;
    //private List<TeamManager> friendlyTeams;
    //team's object lists
    [ReadOnly][SerializeField]
    protected List<BaseObject> objectsList = new List<BaseObject>();
    //List of all enemy that is currently in range
    [ReadOnly][SerializeField]
    protected List<BaseObject> targetList = new List<BaseObject>();

    [ReadOnly][SerializeField]
    protected List<BaseObject> nonTargetedList = new List<BaseObject>();

    [SerializeField]
    private int team;

    [SerializeField]
    public int allowedTypeMin;

    [SerializeField]
    public int allowedTypeMax;

    //cache variables
    private float updateTargetCountUp;
    private float updateTargetRate = 0.15f;

    public delegate void VoidDelegateBaseObject(BaseObject b);
    public VoidDelegateBaseObject OnObjectSpawn;
    public VoidDelegateBaseObject OnObjectStopProvidingSight;
    public VoidDelegateBaseObject OnObjectRemove;

    public VoidDelegate OnTargetListUpdate;

    public override void Init()
    {
        base.Init();

        OnObjectSpawn = (BaseObject b) => { };
        OnObjectStopProvidingSight = (BaseObject b) => { };
        OnObjectRemove = (BaseObject b) => { };
        OnTargetListUpdate = () => { };

        objectsList = new List<BaseObject>();
        gameObject.name = GetType().ToString() + "_" + GetTeam();

        enemyTeams = new List<TeamManager>();
        enemyTeams.AddRange(FindObjectsOfType<TeamManager>());
        enemyTeams.Remove(this);

        //do something with enemy team and friendly team, I don't know :/

        //find all the object
        //and add the object that is fit in the range
		//and init them
        BaseObject[] allObjects = FindObjectsOfType<BaseObject>();
        for (int i = 0; i < allObjects.Length; i++)
        {
            if ((int)allObjects[i].GetObjectType() <= allowedTypeMax
                && (int)allObjects[i].GetObjectType() >= allowedTypeMin)
            {
                objectsList.Add(allObjects[i]);
				allObjects[i].Init(this);
            }
        }
    }

    public override void DoUpdate()
    {
        base.DoUpdate();
        updateTargetCountUp += Time.deltaTime;
        if (updateTargetCountUp >= updateTargetRate)
        {
            updateTargetCountUp = 0f;
            UpdateTargetList();
        }
        for (int i = 0; i < objectsList.Count; i ++)
        {
            objectsList[i].DoUpdate();
        }
    }

    public BaseObject SpawnObject(ObjectType objectType, Vector3 spawnPosition)
    {
        BaseObject baseObject = PrefabsManager.SpawnObject(objectType);
        baseObject.Init(this);
        baseObject.ForceMoveObjectTo(spawnPosition);
        objectsList.Add(baseObject);
        UpdateTargetList();

        OnObjectSpawn(baseObject);
        return baseObject;
    }

    public void RemoveObject(BaseObject baseObject)
    {
        objectsList.Remove(baseObject);
        OnObjectRemove(baseObject);
    }

    //method two, for each enemies team
    //for each of the enemies, its with all of our currents object,
    //this is quite more complicated but save a lot of seconds guess.
    private void UpdateTargetList()
    {
        targetList = new List<BaseObject>();
        nonTargetedList = new List<BaseObject>();
        //old method using sight info. Only work for circular sight.
        /*
        List<ObjectSightInfo> sightInfo = new List<ObjectSightInfo>();
        for (int i = 0; i < objectsList.Count; i++)
        {
            BaseObject b = objectsList[i];
            if (b.CanProvideSight())
            {
                sightInfo.Add(new ObjectSightInfo(b.transform.position, b.sight, b.requireLOS));
            }
        }
        for (int i = 0; i < enemyTeams.Count; i++)
        {
            for (int j = 0; j < enemyTeams[i].GetObjectsList().Count; j++)
            {
                bool isTargetObject = false;
                BaseObject b = enemyTeams[i].GetObjectsList()[j];
                for (int k = 0; k < sightInfo.Count; k++)
                {
                    if ((b.transform.position - sightInfo[k].position).magnitude < sightInfo[k].sight
                        && (!sightInfo[k].requireLOS || LOSHelper.HasLOS(b.transform.position, sightInfo[k].position)))
                    {
                        targetList.Add(b);
                        isTargetObject = true;
                        continue;
                    }
                }
                if (!isTargetObject)
                {
                    nonTargetedList.Add(b);
                }
            }
        }
        */

        List<BaseObject> provideSightObject = new List<BaseObject>();
        for (int i = 0; i < objectsList.Count; i++)
        {
            if (objectsList[i].CanProvideSight())
            {
                provideSightObject.Add(objectsList[i]);
            }
        }
        foreach (TeamManager enemyTeam in enemyTeams)
        {
            foreach(BaseObject enemyObject in enemyTeam.GetObjectsList())
            {
                bool isTargetObject = false;
                //for(int i = 0; i < provideSightObject.Count; i ++)
                foreach (BaseObject sightObject in provideSightObject)
                {
                    if (LOSHelper.CanSeeTarget(sightObject, enemyObject.transform.position)) {
                        targetList.Add(enemyObject);
                        isTargetObject = true;
                        continue;
                    }
                }
                if (!isTargetObject)
                {
                    nonTargetedList.Add(enemyObject);
                }
            }
        }
        OnTargetListUpdate();
    }

    //get the nearest object of this team to a position in the world
    public BaseObject GetNearestObjectTo(Vector3 _position, float maxDistance, bool checkLineOfSight)
    {
        BaseObject returnObject = null;
        float distance = 999f;
        for (int i = 0; i < objectsList.Count; i++)
        {
            if (!objectsList[i].CanBeAttacked())
            {
                continue;
            } else
            {
                float tempDistance = (objectsList[i].transform.position - _position).magnitude;
				//max distance is usually the object sight.
				if (maxDistance > 0f && tempDistance > maxDistance) 
				{
					continue;
				}
                if (tempDistance < distance)
                {
					if (checkLineOfSight) 
					{
                        //move both point a little bit up so we don't touch the ground
                        if (!LOSHelper.HasLOS(_position, objectsList[i].transform.position))
                        //if (Physics.Raycast(_position + new Vector3(0f, 1f, 0f), objectsList[i].transform.position - _position, tempDistance, wallLayer))
                        {
                            continue;
                        } else
                        {
                            returnObject = objectsList[i];
                            distance = tempDistance;
                        }

                    } else 
					{
						returnObject = objectsList[i];
						distance = tempDistance;
					}

                }
            }
        }
        return returnObject;
    }

    //get all the objects in a regions
    public List<BaseObject> GetObjectsInRange(Vector3 _position, float range, bool checkLineOfSight)
    {
        List<BaseObject> returnList = new List<BaseObject>();
        for (int i = 0; i < objectsList.Count; i++)
        {
            if (!objectsList[i].CanBeAttacked())
            {
                continue;
            }
            else
            {
                float tempDistance = (objectsList[i].transform.position - _position).magnitude;
                //max distance is usually the object sight.
                if (tempDistance > range)
                {
                    continue;
                }
                if (checkLineOfSight)
                {
                    //Debug.DrawLine(_position + new Vector3(0f, 1f, 0f), objectsList[i].transform.position + new Vector3(0f, 1f, 0f), Color.blue, 0.5f);
                    //move both point a little bit up so we don't touch the ground
                    if (!LOSHelper.HasLOS(_position, objectsList[i].transform.position))
                    //if (Physics.Raycast(_position + new Vector3(0f, 1f, 0f), objectsList[i].transform.position - _position, tempDistance, wallLayer))
                    {
                        continue;
                    } else
                    {
                        returnList.Add(objectsList[i]);
                    }
                }
                else
                {
                    returnList.Add(objectsList[i]);
                }
            }
        }
        return returnList;
    }

    public virtual BaseObject RequestTarget(AttackComponent attacker)
    {
        Vector3 attackerPosition = attacker.transform.position;
        BaseObject returnObject = null;
        float distance = 999f;
        
        for (int i = 0; i < targetList.Count; i ++)
        {
            if (!targetList[i].CanBeAttacked())
            {
                continue;
            }
            float tempDistance = (attackerPosition - targetList[i].transform.position).magnitude;
            if (tempDistance < distance)
            {
                distance = tempDistance;
                returnObject = targetList[i];
            }
        }
        return returnObject;
    }

    //The object find target on its own, without information from other friendlies
    public virtual BaseObject RequestTargetSingle(AttackComponent attacker) 
	{
        Vector3 attackerPosition = attacker.transform.position;
        BaseObject returnObject = null;
        float distance = 999f;
        
        for (int i = 0; i < enemyTeams.Count; i ++)
        {
            BaseObject closestObject = enemyTeams[i].GetNearestObjectTo(attackerPosition, attacker.agressiveRange, true);
            if (closestObject == null)
            {
                continue;
            }
            float currentDistance = (closestObject.transform.position - attackerPosition).magnitude;
            if (currentDistance < distance)
            {
                distance = currentDistance;
                returnObject = closestObject;
            }
        }
        return returnObject;
	}

    //if a object target an enemy and the enemy run out of sight from his view and 
    //all of the friendly view, then stop attack
    public bool CanSeeTarget(BaseObject b) { return targetList.Contains(b); }

    public List<BaseObject> GetObjectsList() { return objectsList; }

    public List<BaseObject> GetTargetList() { return targetList; }

    public List<BaseObject> GetNonTargetList() { return nonTargetedList; }

    public int GetTeam() { return team; }

    public virtual bool IsObjectFriendly(BaseObject b)
    {
        return (b.GetObjectTeam() == GetTeam());
    }

    //archive stuffs

    //method one, for each enemies team, 
    //run through the list of our current object and find all of his target in sight
    //if a target is not in the target list, add it to the list.
    //this cause a lot of re-calculation, like, if an enemy is in the middle of the team
    //it will be registered and check and exclude from the list a lot
    private void UpdateTargetListOld()
    {
        targetList = new List<BaseObject>();
        for (int i = 0; i < enemyTeams.Count; i++)
        {
            List<BaseObject> thisTeamTargetList = new List<BaseObject>();
            for (int j = 0; j < objectsList.Count; j++)
            {
                if (!objectsList[j].CanProvideSight())
                //if (objectsList[j].GetObjectState() == ObjectState.Die)
                {
                    continue;
                }
                //get all the enemies that is currently in sight of this object
                List<BaseObject> currentTargetList = enemyTeams[i].GetObjectsInRange(objectsList[j].transform.position, objectsList[j].sight, true);

                foreach (BaseObject b in currentTargetList)
                {
                    if (!thisTeamTargetList.Contains(b))
                    {
                        thisTeamTargetList.Add(b);
                    }
                }
            }
            targetList.AddRange(thisTeamTargetList);
        }
    }
}

public struct ObjectSightInfo
{
    public Vector3 position;
    public float sight;
    public bool requireLOS;

    public ObjectSightInfo(Vector3 v, float s, bool b)
    {
        position = v;
        sight = s;
        requireLOS = b;
    }
}
