﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraManager : BaseManager {

    private InputManager inputManager;

    [SerializeField]
    private float cameraMoveSpeed = 2f;
    [SerializeField]
    private float minCameraZoomIn = 12.5f;
    [SerializeField]
    private float maxCameraZoomOut = 45f;

    private Camera mainCamera;
    private Camera fowCamera;

    private bool isCameraOrthographic;

    //the camera will automatically follow an object.
    [ReadOnly][SerializeField]
    private Transform followedObject;
    private bool isFollowingObject;

    public override void Init()
    {
        base.Init();
		mainCamera = transform.Find("Main_Camera").GetComponent<Camera>();
        fowCamera = GameObject.Find("FOWCam").GetComponent<Camera>();

        isCameraOrthographic = mainCamera.orthographic;
        isFollowingObject = false;
        followedObject = null;

        inputManager = director.inputManager;

        inputManager.OnObjectSelected += (SelectableObject s) =>
        {
            SetFollowObject(s.transform);
        };

        inputManager.OnObjectDeSelected += (SelectableObject s) =>
        {
            if (followedObject == s.transform)
            {
                SetFollowObject(null);
            }
        };
    }

    public override void DoUpdate()
    {
        base.DoUpdate();
        if (isFollowingObject)
        {
            //FollowTarget();
        } else
        {
            CheckMovementInput();
        }
        CheckZoomInput();
    }

    void LateUpdate()
    {
        if (isFollowingObject)
        {
            FollowTarget();
        }
    }

    public void SetFollowObject(Transform _object)
    {
        followedObject = _object;
        isFollowingObject = followedObject != null;
    }

    private void FollowTarget()
    {
        //Vector3 destinatedPosition = new Vector3(followedObject.transform.position.x, mainCamera.transform.position.y, followedObject.transform.position.z);
        //mainCamera.transform.position = Vector3.MoveTowards(mainCamera.transform.position, destinatedPosition, Time.deltaTime * cameraMoveSpeed * 5f);

        mainCamera.transform.DOMoveX(followedObject.transform.position.x, 0.25f, false);
        mainCamera.transform.DOMoveZ(followedObject.transform.position.z, 0.25f, false);
    }

    private void CheckMovementInput()
    {
        if (Input.GetKey(KeyCode.LeftArrow)) { mainCamera.transform.Translate(new Vector3(-1f,0f,0f) * cameraMoveSpeed * Time.deltaTime, Space.World); }
        if (Input.GetKey(KeyCode.RightArrow)) { mainCamera.transform.Translate(new Vector3(1f, 0f, 0f) * cameraMoveSpeed * Time.deltaTime, Space.World); }
        if (Input.GetKey(KeyCode.UpArrow)) { mainCamera.transform.Translate(new Vector3(0f, 0f, 1f) * cameraMoveSpeed * Time.deltaTime, Space.World); }
        if (Input.GetKey(KeyCode.DownArrow)) { mainCamera.transform.Translate(new Vector3(0f, 0f, -1f) * cameraMoveSpeed * Time.deltaTime, Space.World); }
    }

    private void CheckZoomInput()
    {
        if (Input.GetKey(KeyCode.Home))
        {
            //zoomin = true
            CameraZoom(true);
        }

        if (Input.GetKey(KeyCode.End))
        {
            //zoomin = false
            CameraZoom(false);
        }

        if (Input.mouseScrollDelta.y != 0)
        {
            CameraZoom(Input.mouseScrollDelta.y > 0, Mathf.Abs(Input.mouseScrollDelta.y) * 0.1f / Time.deltaTime);
        }
    }

    public Camera GetMainCamera() { return mainCamera; }

    private void CameraZoom(bool zoomIn, float speedMultiplier = 1f)
    {
        int cameraZoomSign = zoomIn ? -1 : 1;
        if (isCameraOrthographic)
        {
            mainCamera.orthographicSize += cameraZoomSign * Time.deltaTime * cameraMoveSpeed / 2f * speedMultiplier;
            fowCamera.orthographicSize += cameraZoomSign * Time.deltaTime * cameraMoveSpeed / 2f * speedMultiplier;
        } else
        {
            if ((zoomIn && mainCamera.transform.position.y >= minCameraZoomIn)
                || (!zoomIn && mainCamera.transform.position.y <= maxCameraZoomOut))
            {
                mainCamera.transform.Translate(new Vector3(0f, cameraZoomSign, 0f) * cameraMoveSpeed * Time.deltaTime * speedMultiplier, Space.World);
            }
        }
    }
}
