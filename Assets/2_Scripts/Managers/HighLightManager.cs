﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighLightManager : BaseManager {

    public TargetCircle circlePrefab;

    [SerializeField]
    private List<TargetCircle> selectedCircle;

    [SerializeField]
    private TargetCircle destinatedTarget;

    //for pooling
    [SerializeField]
    private List<TargetCircle> inactiveCircleList;

    public override void Init()
    {
        base.Init();
        selectedCircle = new List<TargetCircle>();
        inactiveCircleList = new List<TargetCircle>();

        destinatedTarget = Instantiate(circlePrefab);
        destinatedTarget.SetColor(Ultilities.HexToColor("D5E314C3"));
        destinatedTarget.HideCircle();
    }

    public void SelectObject(SelectableObject s)
    {
        TargetCircle myTarget;
        if (inactiveCircleList.Count == 0)
        {
            myTarget = Instantiate(circlePrefab);
            myTarget.SetColor(new Color(1f, 1f, 1f, 0.85f));
        } else
        {
            myTarget = inactiveCircleList[0];
            inactiveCircleList.RemoveAt(0);
        }
        selectedCircle.Add(myTarget);
        myTarget.SetTarget(s.gameObject);
    }

    public void SelectObjects(SelectableObject[] sArray)
    {
        
    }

    public void DeslectObject(SelectableObject s)
    {
        TargetCircle t = null;
        for (int i = 0; i < selectedCircle.Count; i ++)
        {
            if (selectedCircle[i].GetTargetGameObject() == s.gameObject)
            {
                t = selectedCircle[i];
                break;
            }
        }
        if (t != null)
        {
            selectedCircle.Remove(t);
            inactiveCircleList.Add(t);
            t.HideCircle();
        }
    }

    public void MovementPosHighLight(Vector3 pos)
    {
        destinatedTarget.SetPosition(pos);
    }
}
