﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWarManager : BaseManager {
	//private Camera fowCamera;
	//the team that will watching
	private TeamManager revealingTeam;

    [SerializeField]
    private Renderer fowPlane;

    [ReadOnly][SerializeField]
    private List<CircleFogMask> fowMaskList;

	public override void Init ()
	{
        base.Init();
        //fowCamera = GameObject.Find("FOWCam").GetComponent<Camera>();
		fowMaskList = new List<CircleFogMask>();
        fowPlane.gameObject.SetActive(true);

        SetRevealingTeam(director.playerManager);
	}

    public void SetFowAlphaValue(int alphaValue)
    {
        fowPlane.material.SetColor("_Color", new Color(0f,0f,0f, (float)alphaValue/255));
    }

	public void SetRevealingTeam(TeamManager team) 
	{
        if (revealingTeam != null)
        {
            revealingTeam.OnObjectSpawn -= SetMaskToObject;
            revealingTeam.OnObjectRemove -= OnObjectStopProvidingSight;
            revealingTeam.OnTargetListUpdate -= OnTargetListUpdate;
        }

        revealingTeam = team;
        revealingTeam.OnObjectSpawn += SetMaskToObject;
        revealingTeam.OnObjectStopProvidingSight += OnObjectStopProvidingSight;
        revealingTeam.OnTargetListUpdate += OnTargetListUpdate;

        foreach (CircleFogMask r in fowMaskList) 
		{
            r.HideMask();
		}
		fowMaskList = new List<CircleFogMask>();

        foreach(BaseObject b in team.GetObjectsList())
        {
            SetMaskToObject(b);
            b.ShowRenderer();
        }
	}

	public override void DoUpdate ()
	{
		base.DoUpdate();

		if (Input.GetKeyDown(KeyCode.I)) 
		{
			SetRevealingTeam(director.playerManager);
		}
		if (Input.GetKeyDown(KeyCode.J)) 
		{
			SetRevealingTeam(director.aiManager);
		}
    }

    private void OnObjectStopProvidingSight(BaseObject b)
    {
        for (int i = 0; i < fowMaskList.Count; i++)
        {
            if (fowMaskList[i].targetObject == b)
            {
                fowMaskList[i].HideMask();
                fowMaskList.RemoveAt(i);
                return;
            }
        }
    }

    private void SetMaskToObject(BaseObject bo)
    {
        switch(bo.fogMaskType)
        {
            case FogMaskType.None:
            default:
                //don't do anything
                break;
            case FogMaskType.Circle:
                CircleFogMask circleFogMask = CircleFogMask.SpawnCircleMask();
                fowMaskList.Add(circleFogMask);
                circleFogMask.Init(bo);
                break;
            case FogMaskType.Circle_Line_Of_Sight:
                CircleLOSFogMask losFogMask = CircleLOSFogMask.SpawnFogMask();
                fowMaskList.Add(losFogMask);
                losFogMask.Init(bo);
                break;
            case FogMaskType.Field_Of_View:
                FOVFogMask fovFogMask = FOVFogMask.SpawnFOVFogMask();
                fowMaskList.Add(fovFogMask);
                fovFogMask.Init(bo);
                break;
        }
    }

    private void OnTargetListUpdate()
    {
        foreach (BaseObject b in revealingTeam.GetNonTargetList())
        {
            b.HideRenderer();
        }
        foreach(BaseObject b in revealingTeam.GetTargetList())
        {
            b.ShowRenderer();
        }
    }
}

public enum FogMaskType
{
    None = 0,
    Circle,
    Circle_Line_Of_Sight,
    Field_Of_View
}