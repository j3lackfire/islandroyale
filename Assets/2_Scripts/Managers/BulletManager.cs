﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : BaseManager {

    [ReadOnly][SerializeField]
    private List<BaseBullet> bulletList = new List<BaseBullet>();

    public BaseBullet SpawnBullet(AttackComponent _attacker, BaseWeapon _baseWeapon, Vector3 _startPos, Vector3 _endPos)
    {
        BaseBullet bullet = PrefabsManager.SpawnPrefab<BaseBullet>("BaseBullet", "Prefabs/Dynamic/Weapons/");
        bullet.Init(_attacker, _baseWeapon, _startPos, _endPos);
        bulletList.Add(bullet);
        return bullet;
    }

    public void RemoveBullet(BaseBullet b)
    {
        bulletList.Remove(b);
    }

    public override void Init()
    {
        base.Init();
        bulletList = new List<BaseBullet>();
    }

    public override void DoUpdate()
    {
        base.DoUpdate();
        for (int i = bulletList.Count -1; i >= 0; i --)
        {
            bulletList[i].DoUpdate();
        }
    }
}

public enum AmmoType
{
    Invalid,
    No_Ammo,
    Pistol,
    SMG,
    Rifle,
    MachineGun,
    Rocket,
    Shotgun,
    Sniper
}
