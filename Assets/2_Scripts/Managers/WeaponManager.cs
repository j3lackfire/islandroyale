﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : BaseManager {

    //[ReadOnly][SerializeField]
    //private List<BaseWeapon> weaponList = new List<BaseWeapon>();

    [ReadOnly]
    [SerializeField]
    private List<PickUpWeapon> pickupList = new List<PickUpWeapon>();

    public BaseWeapon SpawnWeapon(WeaponType weaponType)
    {
        return PrefabsManager.SpawnPrefab<BaseWeapon>(weaponType.ToString(), "Prefabs/Dynamic/Weapons/");
    }

    public PickUpWeapon SpawnPickUp(WeaponType weaponType, Vector3 spawnedPosition)
    {
        PickUpWeapon p = SpawnPickUp(weaponType, spawnedPosition, Vector3.zero);
        p.RandomizedPosition();
        return p;
    }

    public PickUpWeapon SpawnPickUp(WeaponType weaponType, Vector3 spawnedPosition, Vector3 spawnedRotation)
    {
        PickUpWeapon p = PrefabsManager.SpawnPrefab<PickUpWeapon>("PickUp_" + weaponType.ToString(), "Prefabs/Dynamic/Weapons/PickUp/");
        p.SetPosition(spawnedPosition);
        p.transform.localRotation = Quaternion.Euler(spawnedRotation);
        pickupList.Add(p);
        return p;
    }

    public void RemovePickUp(PickUpWeapon p)
    {
        pickupList.Remove(p);
    }

    public AnimatorOverrideController GetWeaponOverrideController(WeaponType weaponType)
    {
        string animatorName;
        switch (weaponType)
        {
            default:
            case WeaponType.Unequipped:
                animatorName = "Weapon_None";
                break;
            case WeaponType.Pistol_Glock:
                animatorName = "Weapon_Pistol";
                break;
            case WeaponType.Rifle_AK:
            case WeaponType.Rifle_M4:
                animatorName = "Weapon_Rifle";
                break;
            case WeaponType.SMG_Uzi:
                animatorName = "Weapon_Pistol";
                break;
        }
        return Resources.Load<AnimatorOverrideController>("Animators/" + animatorName);
    }
}

public enum WeaponType
{
    Unequipped = -1,
    Pistol_Glock = 0,
    Rifle_AK = 100,
    Rifle_M4 = 101,
    SMG_Uzi = 200,
    Melee_Fire_Axe = 1001
}
