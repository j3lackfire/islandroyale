﻿using UnityEngine;

//all the delegate that I'm going to use
//muti purpose, use in multiple place
public delegate void VoidDelegate();

//use for targetting object and position
public delegate void VoidDelegateVector3(Vector3 v);
public delegate void VoidDelegateBaseObject(BaseObject b);
public delegate void VoidDelegateSelectableObject(SelectableObject s);
public delegate void VoidDelegateInteractiveComponent(InteractiveComponent i);
public delegate void VoidDelegateBaseWeapon(BaseWeapon b);
public delegate void VoidDelegateInt(int i);
public delegate void VoidDelegateInteractbleObject(InteractableObject i);


//boolean delegate
public delegate bool BoolDelegate();

public delegate void VoidDelegateDamageReceive(int damage, AttackComponent attacker);
public delegate void VoidDelegateStateChange(ObjectState state);
