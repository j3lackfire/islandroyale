﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LOSHelper {

    private static int wallLayer;
    //private static int groundLayer;
    //private static int selectableObjectLayer;
    //private static int baseObjectLayer;

    private static int wall;
    private static int ground;
    private static int selectableObject;
    private static int baseObject;

    private static int bulletHitLayer;

    private static Vector3 headPos = new Vector3(0, 2.5f, 0);
    //will be used when unit can sit behind cover to hide
    //private static Vector3 sitPos = new Vector3(0, 0.55f, 0);

    public static void PrepareLOSHelper()
    {
        wall = LayerMask.NameToLayer("Wall");
        ground = LayerMask.NameToLayer("Ground");
        selectableObject = LayerMask.NameToLayer("SelectableObject");
        baseObject = LayerMask.NameToLayer("BaseObject");

        wallLayer = 1 << LayerMask.NameToLayer("Wall");

        bulletHitLayer = 1 << wall
            | 1 << ground
            | 1 << selectableObject
            | 1 << baseObject;

    }

    public static bool HasLOS(Vector3 pos, Vector3 target, int layer)
    {
        //Debug.DrawLine(_position + new Vector3(0f, 1f, 0f), objectsList[i].transform.position + new Vector3(0f, 1f, 0f), Color.blue, 0.5f);
        return !Physics.Raycast(pos + headPos, target - pos, (target - pos).magnitude, layer);
    }

    public static bool HasLOS(Vector3 pos, Vector3 target)
    {
        return HasLOS(pos, target, wallLayer);
    }

    public static bool CanSeeTarget(BaseObject baseObject, Vector3 target)
    {
        //if target is within field of view, compare the distance between the target and sight of the object.
        //if not compare it to the back-sight of the object
        float testDistance = IsPositionInFieldOfView(baseObject, target) ? baseObject.sight : baseObject.backSight;
        //the object can actually see the target if it is within the distance range, and there is nothing block the view.
        return (((target - baseObject.transform.position).magnitude <= testDistance)
            && HasLOS(baseObject.transform.position, target));
    }

    public static float CalculateAngle(Transform baseTransform, Vector3 pos)
    {
        return Vector3.Angle(baseTransform.forward, pos - baseTransform.position);
    }

    public static bool IsPositionInFieldOfView(BaseObject baseObject, Vector3 targetPos)
    {
        return CalculateAngle(baseObject.transform, targetPos) <= (baseObject.fieldOfView/2);
    }

    //implement check frienly fire, if non-friendly fire then bullet pass through friendly object
    public static bool CheckBulletHit(Vector3 startPos, Vector3 direction, float length, out BaseObject hitObject)
    {
        RaycastHit hit;
        if (Physics.Raycast(startPos, direction, out hit, length, bulletHitLayer))
        {
            if (hit.transform.gameObject.layer == selectableObject ||
                hit.transform.gameObject.layer == baseObject)
            {
                hitObject = hit.transform.GetComponent<BaseObject>();
            } else
            {
                hitObject = null;
            }
            return true;
        } else
        {
            hitObject = null;
            return false;
        }
    }

}
