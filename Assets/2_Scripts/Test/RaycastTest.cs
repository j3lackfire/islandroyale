﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastTest : MonoBehaviour {

    public GameObject go1;
    public GameObject go2;

    float count = 0f;

    int layer = 0;

    void Awake()
    {
        layer = 1 << LayerMask.NameToLayer("Wall");
    }

    public void Update()
    {
        count += Time.deltaTime;
        if (count >= 0.1f)
        {
            count = 0f;
            Ray ray = new Ray(go1.transform.position, go2.transform.position - go1.transform.position);
            RaycastHit hit;
            Debug.DrawRay(go1.transform.position, go2.transform.position - go1.transform.position, Color.red, 0.5f);
            if (Physics.Raycast(ray,out hit, Mathf.Infinity, layer))
            {
                Debug.Log("<color=blue>hit </color>" + hit.transform.name, hit.transform.gameObject);
            } else
            {
                Debug.Log("Not hit");
            }
        }
        
        
    }
}
