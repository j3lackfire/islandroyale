﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//a full 360 fog mask check that does not have Field of view check
public class CircleFogMask : PooledObject {

    public static CircleFogMask SpawnCircleMask()
    {
        return PrefabsManager.SpawnPrefab<CircleFogMask>("Circle_Fog_Mask", "Prefabs/Effects/");
    }
    
    public BaseObject targetObject;

    [ReadOnly][SerializeField]
    protected float viewRadius = 10f;
    [ReadOnly][SerializeField]
    protected float backViewRaius = 7f;
    [ReadOnly][SerializeField]
    protected float fieldOfView = 360;

    public virtual void Init(BaseObject _targetObject)
    {
        gameObject.SetActive(true);
        targetObject = _targetObject;
        viewRadius = _targetObject.sight;
        backViewRaius = _targetObject.backSight;
        fieldOfView = _targetObject.fieldOfView;
        transform.localScale = new Vector3(viewRadius, 0.1f, viewRadius);
    }

    protected virtual void LateUpdate()
    {
        FollowTargetObject();
    }

    public void HideMask()
    {
        ReturnToPool();
    }

    protected virtual void FollowTargetObject()
    {
        transform.position = new Vector3(targetObject.transform.position.x, Ultilities.fogMaskYPos, targetObject.transform.position.z);
    }
}
