﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleLOSFogMask : CircleFogMask {
    public static CircleLOSFogMask SpawnFogMask()
    {
        return PrefabsManager.SpawnPrefab<CircleLOSFogMask>("Circle_LOS_Fog_Mask", "Prefabs/Effects/");
    }

    protected MeshFilter meshFilter;
    protected Mesh viewMesh;

    protected MeshCollider meshCollider;

    [SerializeField]
    protected float meshResolution;
    [SerializeField]
    protected int edgeResolveIterations;
    [SerializeField]
    protected float edgeDistanceThreshold;
    [SerializeField]
    protected float maskCutawayDistance = 0.1f;

    //protected Vector3 targetLastPos;
    protected int wallLayerMask;

    protected float redrawMaskCountDown;
    protected float redrawMaskTime = 0.25f;

    protected virtual void Awake()
    {
        wallLayerMask = 1 << LayerMask.NameToLayer("Wall");
        meshFilter = GetComponent<MeshFilter>();
        meshCollider = GetComponent<MeshCollider>();
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        meshFilter.mesh = viewMesh;
    }

    public override void Init(BaseObject _targetObject)
    {
        base.Init(_targetObject);
        transform.localPosition = Vector3.zero;
		transform.localScale = Vector3.one;
        DrawFieldOfView();
        //targetLastPos = targetObject.transform.position;
        redrawMaskCountDown = redrawMaskTime;

        fieldOfView = 360;
    }

    protected override void LateUpdate() 
	{
        base.LateUpdate();
        DrawFieldOfView();

        /*
        This is a optimization to make it does not draw the field of view every frame if the object is not moving.
        How ever, if the object is turning, it feels very laggy, which make it really agaisnt the point,
        of making the game feels laggy, which the point of the optimization is to just make it feel more smooth
        */
        //if ((targetLastPos - targetObject.transform.position).magnitude >= 0.1f)
        //{
        //    targetLastPos = targetObject.transform.position;
        //    DrawFieldOfView();
        //}
        //else
        //{
        //    redrawMaskCountDown -= Time.deltaTime;
        //    if (redrawMaskCountDown <= 0f)
        //    {
        //        redrawMaskCountDown = redrawMaskTime;
        //        DrawFieldOfView();
        //    }
        //}
    }

    protected virtual void DrawFieldOfView()
    {
        //how many lines will we draw
		int stepCount = Mathf.RoundToInt(fieldOfView * meshResolution);
        //what is the angle between each step
		float stepAngleSize = fieldOfView / stepCount;
        //all the view points
		List<Vector3> viewPoints = new List<Vector3>();
        //cached value
		ViewCastInfo oldViewCast = new ViewCastInfo();
        for (int i = 0; i <= stepCount; i++)
        {
            float angle = targetObject.transform.eulerAngles.y - fieldOfView / 2 + stepAngleSize * i;
            ViewCastInfo newViewCast = ViewCast(angle);
            
			if (i > 0)
            {
                //fix rare case where the unit look at two different wall the block los
                CheckEdgeWall(oldViewCast, newViewCast, viewPoints);
            }
            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }
        DrawViewMesh(viewPoints);
    }

    protected EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for (int i = 0; i < edgeResolveIterations; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle, (minViewCast.distance + maxViewCast.distance) / 2f);

            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.distance - newViewCast.distance) > edgeDistanceThreshold;
            if (newViewCast.isHit == minViewCast.isHit && !edgeDstThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    protected ViewCastInfo ViewCast(float globalAngle, float _radius  = -1f)
    {
        float radius = _radius == -1f ? viewRadius : _radius; 

        Vector3 direction = DirectionFromAngle(globalAngle, true);
        RaycastHit hit;

		if (Physics.Raycast(transform.position + new Vector3(0f, 0f, 0f), direction, out hit, radius, wallLayerMask))
        {
            //Debug.DrawLine(transform.position + new Vector3(0f, 0f, 0f), hit.point, Color.red, 0.01f);
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        } else
        {
            //Debug.DrawLine(transform.position + new Vector3(0f, 0f, 0f), transform.position + new Vector3(0f, 0f, 0f) + dir.normalized * viewRadius, Color.red, 0.01f);
            return new ViewCastInfo(false, transform.position + direction * radius, radius, globalAngle);
        }
    }

    protected Vector3 DirectionFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    protected override void FollowTargetObject()
    {
        transform.rotation = Quaternion.Euler(Vector3.zero);
        transform.position = new Vector3(targetObject.transform.position.x, Ultilities.fogMaskYPos, targetObject.transform.position.z);
    }

    protected void CheckEdgeWall(ViewCastInfo oldViewCast, ViewCastInfo newViewCast, List<Vector3> viewPoints)
    {
        bool edgeDisttanceThresholdExceeded = Mathf.Abs(oldViewCast.distance - newViewCast.distance) > edgeDistanceThreshold;
        if (oldViewCast.isHit != newViewCast.isHit || (oldViewCast.isHit && newViewCast.isHit && edgeDisttanceThresholdExceeded))
        {
            EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
            if (edge.pointA != Vector3.zero)
            {
                viewPoints.Add(edge.pointA);
            }
            if (edge.pointB != Vector3.zero)
            {
                viewPoints.Add(edge.pointB);
            }
        }
    }

    protected void DrawViewMesh(List<Vector3> viewPoints)
    {
        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]) + Vector3.forward * maskCutawayDistance;
            //			vertices[i + 1] = viewPoints[i] + Vector3.forward * maskCutawayDistance;

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }
        
        viewMesh.Clear();

        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
        meshCollider.sharedMesh = viewMesh;
    }
}


public struct ViewCastInfo
{
    public bool isHit;
    public Vector3 point;
    public float distance;
    public float angle;

    public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
    {
        isHit = _hit;
        point = _point;
        distance = _dst;
        angle = _angle;
    }
}

public struct EdgeInfo
{
    public Vector3 pointA;
    public Vector3 pointB;

    public EdgeInfo(Vector3 _pointA, Vector3 _pointB)
    {
        pointA = _pointA;
        pointB = _pointB;
    }
}