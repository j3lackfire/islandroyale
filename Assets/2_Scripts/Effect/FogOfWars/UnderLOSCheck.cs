﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//check if this object is being seen by the LOS giver or not
public class UnderLOSCheck : MonoBehaviour {

    public VoidDelegate OnObjectReveal = () => { };
    public VoidDelegate OnObjectHidden = () => { };

    [ReadOnly][SerializeField]
    private List<Collider> myColliders;

    private float checkForInactiveColliderCountDown = 0.3f;

    void Awake()
    {
        myColliders = new List<Collider>();
    }

    void Update()
    {
        checkForInactiveColliderCountDown -= Time.deltaTime;
        if (checkForInactiveColliderCountDown <= 0f)
        {
            checkForInactiveColliderCountDown = 0.3f;
            //Count from high to low because if we remove element while counting, it might cause problem.
            for (int i = myColliders.Count - 1; i >= 0; i --)
            {
                if (myColliders[i] == null || !myColliders[i].gameObject.activeInHierarchy)
                {
                    RemoveColliderFromSeenList(myColliders[i]);
                }
            }
        }
    }

    public bool IsBeingSeen() { return myColliders.Count > 0; }

    private void OnTriggerEnter(Collider c)
    {
        if (!IsBeingSeen())
        {
            OnObjectReveal();
            //Debug.Break();
        }
        myColliders.Add(c);
    }

    private void OnTriggerExit(Collider c)
    {
        RemoveColliderFromSeenList(c);
    }

    private void RemoveColliderFromSeenList(Collider c)
    {
        if (!myColliders.Contains(c))
        {
            Debug.Log("<color=red> Error removing collider because it does not exist - </color>" + c.gameObject.name);
            Debug.Break();
        } else
        {
            myColliders.Remove(c);
            if (myColliders.Count == 0)
            {
                OnObjectHidden();
            }
        }
    }
}
