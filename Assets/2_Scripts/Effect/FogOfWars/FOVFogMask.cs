﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Fiew of view fog mask
public class FOVFogMask : CircleLOSFogMask
{
    public static FOVFogMask SpawnFOVFogMask()
    {
        return PrefabsManager.SpawnPrefab<FOVFogMask>("FOV_Fog_Mask", "Prefabs/Effects/");
    }

    public override void Init(BaseObject _targetObject)
    {
        base.Init(_targetObject);
        fieldOfView = _targetObject.fieldOfView;
    }

    protected override void DrawFieldOfView()
    {
        //the long/extended view angle
        int longStepCount = Mathf.RoundToInt(fieldOfView * meshResolution);
        int shortStepCount = Mathf.RoundToInt(360 * meshResolution) - longStepCount;
        int totalStepCount = (longStepCount + shortStepCount);

        //what is the angle between each step
        float stepAngleSize = 360 / (float)totalStepCount;

        //all the view points
        List<Vector3> viewPoints = new List<Vector3>();

        //cached value
        ViewCastInfo oldViewCast = new ViewCastInfo();

        int startViewIndex = shortStepCount / 2;
        int endViewIndex = startViewIndex + longStepCount;

        for (int i = 0; i <= totalStepCount; i++)
        {
            ViewCastInfo newViewCast;
            //float angle = transform.eulerAngles.y - viewAngle / 2 + stepAngleSize * i;
            float angle = targetObject.transform.eulerAngles.y - 180 + stepAngleSize * i; // 180 = view angle (

            if (i > 0)
            {
                //the big view where object is looking at
                if (i >= startViewIndex && i < endViewIndex)
                {
                    newViewCast = ViewCast(angle);
                } else //the small view around the object there it's not looking at
                {
                    newViewCast = ViewCast(angle, backViewRaius);
                }
                //fix rare case where the unit look at two different wall the block los
                CheckEdgeWall(oldViewCast, newViewCast, viewPoints);
            } else
            {
                newViewCast = ViewCast(angle, backViewRaius);
            }
            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }
        DrawViewMesh(viewPoints);
    }
}
