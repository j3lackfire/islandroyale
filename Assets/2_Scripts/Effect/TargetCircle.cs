﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TargetCircle : MonoBehaviour
{
    public GameObject targetGameObject;

    public SpriteRenderer circleSprite;
    public SpriteRenderer pointSprite;
    public Color circleColor;

    [SerializeField]
    private float circleRotateTime = 4f; //how many seconds it takes to rotate 360 degree.

    private float circleSize;
    private Sequence circleStartingSequence;

    private void Awake()
    {
        circleSize = circleSprite.transform.localScale.x;
        SetColor(circleColor);
    }

    private void Update() { RotateCircle(); }

    public void SetTarget(GameObject target) 
    {
        transform.parent = target.transform;
        transform.localPosition = Vector3.zero;
        targetGameObject = target;
        ShowCircle();
        StartAnimationSequence();
    }

    public GameObject GetTargetGameObject() { return targetGameObject; }

    public void SetPosition(Vector3 position)
    {
        targetGameObject = null;
        transform.parent = null;
        transform.position = position;
        ShowCircle();
        StartMovementSequence();
    }

    public void ShowCircle() { gameObject.SetActive(true); }

    public void HideCircle()
    {
        circleStartingSequence.Kill();
        gameObject.SetActive(false);
		transform.parent = null;
    }

    public void SetColor(Color color)
    {
        circleSprite.color = color;
        pointSprite.color = color;
    }

    #region Animation
    private void RotateCircle()
    {
        float currentYRotation = circleSprite.transform.localEulerAngles.y;
        currentYRotation += Time.deltaTime * 360f / circleRotateTime;
        if (currentYRotation >= 360f)
        {
            currentYRotation -= 360f;
        }
        circleSprite.transform.localRotation = Quaternion.Euler(new Vector3(90f, currentYRotation, 0f));
    }

    private void StartAnimationSequence()
    {
        circleSprite.transform.localScale = Vector3.one * 1.5f;
        circleStartingSequence.Kill();
        circleStartingSequence = DOTween.Sequence().OnComplete(() => OnStartingSequenceCompleted());
        circleStartingSequence.Append(circleSprite.transform.DOScale(circleSize * 1.5f, 0.1f));
        circleStartingSequence.Append(circleSprite.transform.DOScale(circleSize * 0.85f, 0.2f));
        circleStartingSequence.Append(circleSprite.transform.DOScale(circleSize, 0.2f));
    }

    private void OnStartingSequenceCompleted()
    {
        circleStartingSequence.Kill();
        circleStartingSequence = DOTween.Sequence();
        circleStartingSequence.Append(circleSprite.transform.DOScale(circleSize * 1.2f, 1f));
        circleStartingSequence.Append(circleSprite.transform.DOScale(circleSize, 1f));
        circleStartingSequence.SetLoops(-1, LoopType.Yoyo);
        //circleSprite.transform.DOScale(circleSize * 1.2f, 1f).SetLoops(-1, LoopType.Yoyo);
    }

    private void StartMovementSequence()
    {
        circleSprite.transform.localScale = Vector3.one * 1.5f;
        circleStartingSequence.Kill();
        circleStartingSequence = DOTween.Sequence().OnComplete(() => HideCircle());
        circleStartingSequence.Append(circleSprite.transform.DOScale(circleSize * 1.5f, 0.1f));
        circleStartingSequence.Append(circleSprite.transform.DOScale(circleSize * 0.85f, 0.2f));
        circleStartingSequence.Append(circleSprite.transform.DOScale(circleSize * 0.2f, 0.45f));
    }
    #endregion

}
