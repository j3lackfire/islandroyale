﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBar : MonoBehaviour {
    protected ObjectRenderer objectRenderer;
    protected Renderer bar;
    [SerializeField] //should be green
    protected Color barColor = new Color(1f, 0f, 0f, 0.7f);
    [SerializeField] //should be yellow
    protected Color middleBarColor = new Color(1f, 1f, 0f, 0.7f);
    [SerializeField] //red
    protected Color emptyBarColor = new Color(1f, 0f, 0f, 0.7f);

    [SerializeField]
    protected bool autoHideBar = false;

	protected Color tempColor;
    //Need to be vector 3 since value can be negative
	protected Vector3 deltaColorFullToMiddle;
	protected Vector3 deltaColorMiddleToDie;
	protected float alpha = 0.7f;

	protected bool isBarShow = true;
	protected Vector3 defaultRotation;

    public virtual void InitBar(ObjectRenderer _objectRenderer)
    {
        objectRenderer = _objectRenderer;
        bar = GetComponent<Renderer>();
        SetBarColor(barColor);

        if (defaultRotation == default(Vector3))
        {
            defaultRotation = transform.localRotation.eulerAngles;
        }

        deltaColorFullToMiddle = new Vector3(
            middleBarColor.r - barColor.r,
            middleBarColor.g - barColor.g,
            middleBarColor.b - barColor.b);

        deltaColorMiddleToDie = new Vector3(
            emptyBarColor.r - middleBarColor.r,
            emptyBarColor.g - middleBarColor.g,
            emptyBarColor.b - middleBarColor.b);

        alpha = barColor.a;

        if (autoHideBar) { HideBar(); }
    }

    private void Update()
    {
		transform.rotation = Quaternion.Euler(defaultRotation);
    }

	protected virtual void SetBarColor(Color _color) { bar.material.SetColor("_Color", _color); }

    public virtual void UpdateBar(float percent)
    {
        if (autoHideBar && percent == 1f)
        {
            HideBar();
        } else {
            ShowBar();
        }
		UpdateBarColor(percent);
        //For circle  bar
        bar.material.SetFloat("_Cutoff", Mathf.Max(0f, 1f - percent));
    }

	public void HideBar()
    {
        isBarShow = false;
        bar.gameObject.SetActive(false);
    }

	public void ShowBar()
    {
        if (isBarShow)
        {
            return;
        }
        isBarShow = true;
        bar.gameObject.SetActive(true);
    }

	protected void UpdateBarColor(float percent) {
		if (percent > 0.5f) //Full  to middle bar
		{
			float deltaColorPercent = (1 - percent) * 2;
			tempColor = new Color(
				barColor.r + deltaColorFullToMiddle.x * deltaColorPercent,
				barColor.g + deltaColorFullToMiddle.y * deltaColorPercent,
				barColor.b + deltaColorFullToMiddle.z * deltaColorPercent,
				alpha);
		}
		else // Middle bar to die
		{
			float deltaColorPercent = (0.5f - percent) * 2;
			tempColor = new Color(
				middleBarColor.r + deltaColorMiddleToDie.x * deltaColorPercent,
				middleBarColor.g + deltaColorMiddleToDie.y * deltaColorPercent,
				middleBarColor.b + deltaColorMiddleToDie.z * deltaColorPercent,
				alpha);
		}
		SetBarColor(tempColor);
	}
}
