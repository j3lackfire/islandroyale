﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : ProgressBar {
	private SpriteRenderer spriteRenderer;
	private float defaultXScale;

	public override void InitBar (ObjectRenderer _objectRenderer)
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		defaultXScale = transform.localScale.x;

        base.InitBar(_objectRenderer);
    }

	protected override void SetBarColor (Color _color) { spriteRenderer.color = _color; }

	public override void UpdateBar (float _percent)
	{
        float percent = _percent;
        if (_percent < 0f)
        {
            percent = 0f;
        } else
        {
            if (_percent > 1f)
            {
                percent = 1f;
            }
        }
        //hide and show stuffs
		if (autoHideBar && percent == 1f)
		{
			HideBar();
		} else
		{
			ShowBar();
		}

		UpdateBarColor(percent);
		//For circle  bar
		transform.localScale = new Vector3(percent * defaultXScale, transform.localScale.y,transform.localScale.z);
	}

}
