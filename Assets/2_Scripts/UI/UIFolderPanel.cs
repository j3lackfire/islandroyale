﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFolderPanel : BaseUIElement {
    protected InputManager inputManager;

    //press this button to show/hide content inside it
    [SerializeField]
	protected GameButton showHideContentButton;

	[ReadOnly][SerializeField]
	protected bool isFolderGroupShow;

	//the container of these buttons
	[SerializeField]
	private BaseUIElement buttonFolder;

    private List<GameButton> myButtons;

	public override void Init()
	{
		base.Init();

        inputManager = Director.instance.inputManager;

        showHideContentButton = transform.Find("Show_Hide_Button").GetComponent<GameButton>();
        showHideContentButton.AddListener(OnShowHideButtonClicked);

        buttonFolder = transform.Find("ButtonsFolder").GetComponent<BaseUIElement>();
        myButtons = new List<GameButton>();
        myButtons.AddRange(buttonFolder.transform.GetComponentsInChildren<GameButton>());

        foreach (GameButton b in myButtons)
        {
            b.AddListener(OnFolderedButtonClicked);
        }

        OnElementShow += HideAllElement;
	}

	protected void OnShowHideButtonClicked() 
	{
		if (isFolderGroupShow) 
		{
			FoldAllElement();
		} else 
		{
			ShowAllElement();
		}
	}

    public void ShowAllElement()
    {
        isFolderGroupShow = true;
        buttonFolder.OnElementShow();

        uiManager.GetEditorPanel().HideAllPanelBut(this);
    }

    public void FoldAllElement() 
	{
        isFolderGroupShow = false;
		buttonFolder.OnElementHide();
	}

	private void HideAllElement()
    {
        buttonFolder.gameObject.SetActive(false);
    }

    //so, when a spawm button is active and the input module is changed,
    //we call this function.
    //This function will add delegate so that when this group of control button is
    //hide, by whatever external function, it return the input manager
    //module back to default
    private void OnFolderedButtonClicked()
    {
        showHideContentButton.AddListener(OnButtonsFolderHiden_WhileActive);
        uiManager.OnEditorHiden += OnButtonsFolderHiden_WhileActive;
        buttonFolder.OnElementHide += OnButtonsFolderHiden_WhileActive;
    }

    private void OnButtonsFolderHiden_WhileActive()
    {
        inputManager.SetInputModule(ModuleInputType.Select_Object);
        showHideContentButton.RemoveListener(OnButtonsFolderHiden_WhileActive);
        uiManager.OnEditorHiden -= OnButtonsFolderHiden_WhileActive;
        buttonFolder.OnElementHide -= OnButtonsFolderHiden_WhileActive;
    }

}
