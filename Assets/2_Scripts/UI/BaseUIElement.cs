﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BaseUIElement : MonoBehaviour {
    protected UIManager uiManager;

    public VoidDelegate OnElementShow;
    public VoidDelegate OnElementHide;

    [SerializeField]
    private Vector3 originalSize = new Vector3(1f,1f,1f);
    //tweening sequences
    private Sequence mySequence;

    public virtual void Init()
    {
        uiManager = Director.instance.uiManager;
        OnElementShow += ShowElement;
        OnElementHide += HideElement;
    }

    public virtual void CancelElementEffect() { }

    public void DeactiveElement()
    {
        this.gameObject.SetActive(false);
    }

    protected virtual void ShowElement()
    {
        mySequence.Complete();
        //Debug.Log("<color=yellow>Show element </color>" + transform.name);
        gameObject.SetActive(true);
        gameObject.transform.localScale = new Vector3(0.01f, originalSize.y, originalSize.z);
        mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOScaleX(originalSize.x * 1.05f, 0.12f));
        mySequence.Append(transform.DOScaleX(originalSize.x * 0.95f, 0.12f));
        mySequence.Append(transform.DOScaleX(originalSize.x, 0.12f));
    }

    protected virtual void HideElement()
    {
        mySequence.Complete();
        //Debug.Log("<color=cyan>Hide element</color> " + transform.name);
        mySequence = DOTween.Sequence().OnComplete(() => 
        {
            gameObject.SetActive(false);
            //Debug.Log("Hide element compeleted " + transform.name);
        });
        mySequence.Append(transform.DOScaleX(0.01f, 0.15f));
    }

    protected void ElementJellyEffect()
    {
        mySequence.Complete();
        mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOScale(originalSize.x * 1.15f, 0.12f));
        mySequence.Append(transform.DOScale(originalSize.x * 0.9f, 0.12f));
        mySequence.Append(transform.DOScale(originalSize.x, 0.12f));
    }
}
