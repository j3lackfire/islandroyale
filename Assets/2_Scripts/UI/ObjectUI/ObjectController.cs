﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : BaseObjectUI {
    private CalmControllerButton calmStateButton;

    private InteractControllerButton interactButton;

    public override void Init()
    {
        base.Init();
        calmStateButton = transform.Find("CalmStateButton").GetComponent<CalmControllerButton>();
        interactButton = transform.Find("InteractButton").GetComponent<InteractControllerButton>();
    }

    private void Start()
    {
        OnObjectDeselected(null);
    }

    protected override void OnObjectSelected(SelectableObject _selectableObject)
    {
        base.OnObjectSelected(_selectableObject);
        //calm stuffs
        calmStateButton.SetSelectedCalmObject(_selectableObject.GetComponent<CalmComponent>());
        //interactive stuffs
        interactButton.SetInteractiveComponent(_selectableObject.GetComponent<InteractiveComponent>());
    }

    protected override void OnObjectDeselected(SelectableObject _selectableObject)
    {
        base.OnObjectDeselected(_selectableObject);
        calmStateButton.SetSelectedCalmObject(null);
        interactButton.SetInteractiveComponent(null);
    }

    protected override void SetSelectedObjectToNull()
    {
        base.SetSelectedObjectToNull();
        calmStateButton.SetSelectedCalmObject(null);
        interactButton.SetInteractiveComponent(null);
    }
}
