﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalmControllerButton : GameButton {

    private CalmComponent calmComponent;

    public override void Init()
    {
        base.Init();
        AddListener(ChangeCalmState);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (calmComponent != null)
            {
                ButtonFunction();
                ChangeCalmState();
            }
        }
    }

    public void SetSelectedCalmObject(CalmComponent _calmComponent)
    {
        if (_calmComponent != null)
        {
            calmComponent = _calmComponent;
            OnElementShow();
            AdjustButtonText();
        } else
        {
            OnElementHide();
        }
    }

    private void ChangeCalmState()
    {
        //if (calmComponent != null)
        //{
        //    calmComponent.SwitchCalmState();
        //    AdjustButtonText();
        //}
        //should I do null check ?
        calmComponent.SwitchCalmState();
        AdjustButtonText();
    }

    private void AdjustButtonText()
    {
        SetButtonText(calmComponent.IsBeingCalm() ? "[A]\n----\n<color=#FF0808FF>Enter\nAttack</color>" : "[A]\n----\n<color=#43FF08FF>Enter\nPassive</color>");
    }
}
