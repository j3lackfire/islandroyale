﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the UI that is catered only for controling un object.
public class BaseObjectUI : BaseUIElement {
    protected InputManager inputManager;
    protected SelectableObject selectedObject;

    public override void Init()
    {
        base.Init();

        inputManager = Director.instance.inputManager;
        inputManager.OnObjectSelected += OnObjectSelected;
        inputManager.OnObjectDeSelected += OnObjectDeselected;
    }

    protected virtual void OnObjectSelected(SelectableObject _selectableObject)
    {
        SetSelectedObject(_selectableObject);
    }

    protected virtual void OnObjectDeselected(SelectableObject _selectableObject)
    {
        SetSelectedObjectToNull();
    }

    protected virtual void SetSelectedObject(SelectableObject s)
    {
        selectedObject = s;
    }

    protected virtual void SetSelectedObjectToNull()
    {
        selectedObject = null;
    }

}
