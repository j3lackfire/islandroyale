﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractControllerButton : GameButton {

    private InteractiveComponent interactiveComponent;

    public override void Init()
    {
        base.Init();
        AddListener(OrderUseObject);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (interactiveComponent != null)
            {
                ButtonFunction();
                OrderUseObject();
            }
        }
    }

    public void SetInteractiveComponent(InteractiveComponent _interactiveComponent)
    {
        if (_interactiveComponent != null)
        {
            interactiveComponent = _interactiveComponent;
            interactiveComponent.OnEnter += OnInteractableObjectEnter;
            interactiveComponent.OnExit += OnInteractableObjectExit;
            InteractableObject currentInteractingObject = interactiveComponent.GetCurrentInteractingObject();
            if (currentInteractingObject != null)
            {
                OnInteractableObjectEnter(currentInteractingObject);
            }
        } else
        {
            OnElementHide();
            if (interactiveComponent != null)
            {
                interactiveComponent.OnEnter -= OnInteractableObjectEnter;
                interactiveComponent.OnExit -= OnInteractableObjectExit;
                interactiveComponent = null;
            }
        }
    }

    private void OnInteractableObjectEnter(InteractableObject i)
    {
        OnElementShow();
        SetButtonText("[SPACE]\n----\n" + i.GetActionName() + "\n" + i.GetObjectName());
    }

    private void OnInteractableObjectExit(InteractableObject i)
    {
        //after the on object exit function, if the object still have another object to interact with
        //it will not hide the button.
        //Because this function is called AFTER the remove/add function, so we can only compare this to null
        if (interactiveComponent.GetCurrentInteractingObject() == null)
        {
            OnElementHide();
        }
    }

    private void OrderUseObject()
    {
        //should have a check null function ? Probably not, if the code is tight.
        interactiveComponent.UseObject();
    }
}
