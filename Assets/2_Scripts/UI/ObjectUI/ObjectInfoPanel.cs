﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectInfoPanel : BaseObjectUI {

    [ReadOnly][SerializeField]
    private Text objectName;

    [ReadOnly][SerializeField]
    private Text weapon;

    [ReadOnly][SerializeField]
    private Text ammoCount;

    private GameButton deselectObjectButton;

    private BaseObject selectedBaseObject;
    private WeaponUserComponent selectedWeaponUser;
    private BaseWeapon selectedWeapon;

    public override void Init()
    {
        base.Init();
        objectName = transform.Find("ObjectName").GetComponentInChildren<Text>();
        weapon = transform.Find("Weapon").GetComponentInChildren<Text>();
        ammoCount = transform.Find("AmmoCount").GetComponentInChildren<Text>();

        deselectObjectButton = transform.Find("DeselectObjectButton").GetComponent<GameButton>();
        deselectObjectButton.AddListener(() =>
        {
            if (selectedObject != null)
            {
                inputManager.DeselectObject(selectedObject);
            }
        });
        OnObjectDeselected(null);
    }

    protected override void OnObjectSelected(SelectableObject _selectableObject)
    {
        base.OnObjectSelected(_selectableObject);
        
        objectName.text = selectedBaseObject.transform.name;
        if (selectedWeaponUser != null)
        {
            selectedWeaponUser.OnWeaponEquipped += OnWeaponEquipped;
            OnWeaponEquipped(selectedWeaponUser.GetCurrentWeapon());
        }
    }

    protected override void OnObjectDeselected(SelectableObject _selectableObject)
    {
        if (selectedWeaponUser != null)
        {
            selectedWeaponUser.OnWeaponEquipped -= OnWeaponEquipped;
            selectedWeapon.OnTriggerPulled -= OnWeaponTriggerPulled;
        }
        base.OnObjectDeselected(_selectableObject);

        objectName.text = "-----";
        weapon.text = "-----";
        ammoCount.text = "-----";
    }

    private void OnWeaponEquipped(BaseWeapon b)
    {
        if (selectedWeaponUser == null)
        {
            Debug.Log("<color=red> Selected weapon user NULL | Selected object </color>" + selectedObject.transform.name, selectedObject.gameObject);
        }
        if (selectedWeapon != null)
        {
            selectedWeapon.OnTriggerPulled -= OnWeaponTriggerPulled;
        }
        selectedWeapon = b;
        ammoCount.text = selectedWeapon.GetAmmoCount() == -1 ? "----" : selectedWeapon.GetAmmoCount().ToString();
        weapon.text = selectedWeaponUser.GetCurrentWeaponType().ToString();
        selectedWeapon.OnTriggerPulled += OnWeaponTriggerPulled;
    }

    private void OnWeaponTriggerPulled()
    {
        if (selectedWeapon != null)
        {
            ammoCount.text = selectedWeapon.GetAmmoCount() == -1 ? "----" : selectedWeapon.GetAmmoCount().ToString();
        } else
        {
            Debug.Log("<color=red> Selected weapon NULL | Selected object </color>" + selectedObject.transform.name, selectedObject.gameObject);
        }
    }

    protected override void SetSelectedObject(SelectableObject s)
    {
        base.SetSelectedObject(s);
        selectedBaseObject = s.GetBaseObject();
        selectedWeaponUser = s.GetComponent<WeaponUserComponent>();
    }

    protected override void SetSelectedObjectToNull()
    {
        base.SetSelectedObjectToNull();
        selectedBaseObject = null;
        selectedWeaponUser = null;
        selectedWeapon = null;
    }
}
