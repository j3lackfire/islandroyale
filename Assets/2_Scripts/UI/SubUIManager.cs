﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubUIManager : MonoBehaviour {

    public void InitSubUIManager()
    {
        foreach (BaseUIElement b in transform.GetComponentsInDirectChildren<BaseUIElement>())
        {
            b.Init();
        }
        foreach (SubUIManager s in transform.GetComponentsInDirectChildren<SubUIManager>())
        {
            s.InitSubUIManager();
        }
    }
}
