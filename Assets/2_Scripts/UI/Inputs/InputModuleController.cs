﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputModuleController
{
    protected InputManager inputManager;
    protected HighLightManager highLightManager;
    protected CameraManager cameraManager;
    protected WeaponManager weaponManager;

    private List<SelectableObject> selectedObjects = new List<SelectableObject>();

    //spawn object
    private ObjectType spawnedObjectType;
    private TeamManager spawnTeam;

    //spawn weapon
    private WeaponType spawnedWeaponType;

    //stuffs for mouse calculation
    //cached value so I don't have to declear them everytimes.
    private Ray ray;
    private RaycastHit hit;

    private int hitLayerGround = 0;
    private int hitLayerSelectableObject = 0;
    private int hitLayerBaseObject;

    public InputModuleController()
    {
        inputManager = Director.instance.inputManager;
        highLightManager = Director.instance.highLightManager;
        cameraManager = Director.instance.cameraManager;
        weaponManager = Director.instance.weaponManager;

        selectedObjects = new List<SelectableObject>();

        //layer and stuffs
        int groundLayer = LayerMask.NameToLayer("Ground");
        int selectableObject = LayerMask.NameToLayer("SelectableObject");
        int baseObjectLayer = LayerMask.NameToLayer("BaseObject");
        //to get two layers;
        //hitLayerSelectableObject = 1 << 8 | 1 << 9;
        hitLayerSelectableObject = 1 << selectableObject;
        hitLayerGround = 1 << groundLayer;
        hitLayerBaseObject = 1 << baseObjectLayer;
    }

    public void AssignModule(ModuleInputType type)
    {
        switch (type)
        {
            case ModuleInputType.Select_Object:
            default:
                inputManager.OnLeftClick = SelectObject;
                inputManager.OnRightClick = OrderObject;
                //inputManager.OnRightClick = () => {};
                break;
            //case ModuleInputType.Control_Unit:
            //    inputManager.OnLeftClick = SelectObject;
            //    inputManager.OnRightClick = OrderObject;
            //break;
            case ModuleInputType.Spawn_Object:
                inputManager.OnLeftClick = SpawnObject;
                inputManager.OnRightClick = CancelInputModule;
                break;
            case ModuleInputType.Spawn_Weapon:
                inputManager.OnLeftClick = SpawnWeapon;
                inputManager.OnRightClick = CancelInputModule;
                break;
        }
    }

    public bool IsSelecting(SelectableObject s) { return selectedObjects.Contains(s); }

    public SelectableObject GetSelectedObject()
    {
        return selectedObjects.Count == 0 ? null : selectedObjects[0];
    }

    #region Function_definition
    private void SelectObject()
    {
        //Create a ray
        ray = cameraManager.GetMainCamera().ScreenPointToRay(Input.mousePosition);

        //if the ray cast hit one of my layer
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, hitLayerSelectableObject))
        {
            for (int i = selectedObjects.Count - 1; i >= 0; i -- )
            {
                DeselectObject(selectedObjects[i]);
            }
            selectedObjects.Clear();

            SelectableObject selectedObject = hit.transform.GetComponent<SelectableObject>();
            selectedObject.OnObjectSelected();
            selectedObjects.Add(selectedObject);
            inputManager.OnObjectSelected(selectedObject);
        }
        else if (Physics.Raycast(ray, out hit, Mathf.Infinity, hitLayerGround))
        {
            //do nothing if the user click on the ground.
        }
    }

    public void DeselectObject(SelectableObject s)
    {
        if (IsSelecting(s))
        {
            s.OnObjectDeselected();
            selectedObjects.Remove(s);
            inputManager.OnObjectDeSelected(s);
        }
    }

    private void OrderObject()
    {
        if (selectedObjects.Count == 0)
        {
            return;
        }
        ray = cameraManager.GetMainCamera().ScreenPointToRay(Input.mousePosition);
        int hitLayer = hitLayerBaseObject | hitLayerSelectableObject;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, hitLayer))
        {
            highLightManager.MovementPosHighLight(hit.transform.gameObject.transform.position);
            BaseObject targetObject = hit.transform.GetComponent<BaseObject>();
            foreach (SelectableObject s in selectedObjects)
            {
                s.OnRightClickTarget(targetObject);
            }
        }
        else if (Physics.Raycast(ray, out hit, Mathf.Infinity, hitLayerGround))
        {
            highLightManager.MovementPosHighLight(hit.point);
            //should order the current selected unit to move to the position
            foreach (SelectableObject s in selectedObjects)
            {
                s.OnRightClickOrder(hit.point);
            }
        }
    }

    private void CancelInputModule()
    {
        inputManager.SetInputModule(ModuleInputType.Select_Object);
    }

    public void SetSpawnObjectModule(ObjectType _spawnedObjectType, TeamManager _spawnTeam)
    {
        spawnedObjectType = _spawnedObjectType;
        spawnTeam = _spawnTeam;
        AssignModule(ModuleInputType.Spawn_Object);
    }

    private void SpawnObject()
    {
        ray = cameraManager.GetMainCamera().ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, hitLayerGround))
        {
            spawnTeam.SpawnObject(spawnedObjectType, new Vector3(hit.point.x, 0f, hit.point.z));
        }
    }

    public void SetSpawnWeaponModule(WeaponType _spawnedWeaponType)
    {
        spawnedWeaponType = _spawnedWeaponType;
        AssignModule(ModuleInputType.Spawn_Weapon);
    }

    private void SpawnWeapon()
    {
        ray = cameraManager.GetMainCamera().ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, hitLayerGround))
        {
            weaponManager.SpawnPickUp(spawnedWeaponType, hit.point);
        }
    }

    #endregion
}

public enum ModuleInputType
{
    Invalid = -1,
    Select_Object  = 0, //click to select, right click doesn't do anything
    Control_Unit = 1, //click to select other object, right click to order to move or attack
    Spawn_Object = 2, //click to spawn, right click to cancel
	Spawn_Weapon = 3 //click to spawn a weapon, right click to cancel
}
