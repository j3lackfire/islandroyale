﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//right now, this is also Mouse controller
public class InputManager : BaseManager {

    //go to this class to view the logic
    private InputModuleController inputModuleController;

    public VoidDelegate OnLeftClick;
    public VoidDelegate OnRightClick;

    public VoidDelegateSelectableObject OnObjectSelected;
    public VoidDelegateSelectableObject OnObjectDeSelected;

    public override void Init()
    {
        base.Init();
        inputModuleController = new InputModuleController();
        inputModuleController.AssignModule(ModuleInputType.Select_Object);

        OnObjectSelected = (SelectableObject s) => { };
        OnObjectDeSelected = (SelectableObject s) => { };
    }

    public override void DoUpdate()
    {
        base.DoUpdate();
        //CheckKeyboardInput();
        CheckMouseInput();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            inputModuleController.DeselectObject(inputModuleController.GetSelectedObject());
        }
    }

    //mouse button 0 is the touch position as well.
    private void CheckMouseInput()
    {
        if (IsMouseOverUI())
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            OnLeftClick();
        } else if (Input.GetMouseButtonDown(1))
        {
            OnRightClick();
        }
    }

    private bool IsMouseOverUI () { return EventSystem.current.IsPointerOverGameObject(); }

    public void SetInputModule(ModuleInputType inputModuleType) { inputModuleController.AssignModule(inputModuleType); }

    public void SetSpawnObjectModule(ObjectType _spawnedObjectType, TeamManager _spawnTeam) { inputModuleController.SetSpawnObjectModule(_spawnedObjectType, _spawnTeam); }

	public void SetSpawnWeaponModule(WeaponType _spawnWeaponType) { inputModuleController.SetSpawnWeaponModule(_spawnWeaponType); }

    public void DeselectAll() { inputModuleController.DeselectObject(inputModuleController.GetSelectedObject()); }

    public void DeselectObject(SelectableObject s) { inputModuleController.DeselectObject(s); }

    //private void CheckKeyboardInput()
    //{
    //    if (Input.GetKeyDown(KeyCode.Escape))
    //    {
    //        DeselectAllObject();
    //    }

    //    if (Input.GetKeyDown(KeyCode.S))
    //    {
    //        foreach (SelectableObject s in selectedObjects)
    //        {
    //            s.OnObjectStopped();
    //        }
    //    }
    //}
}
