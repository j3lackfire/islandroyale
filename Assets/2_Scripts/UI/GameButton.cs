﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameButton : BaseUIElement {

    [SerializeField]
	private Button _myButton;
    private Button myButton
    {
        get
        {
            if (_myButton == null)
            {
                _myButton = GetComponent<Button>();
            }
            return _myButton;
        }
    }

    private Text displayText;
    private Image buttonImage; 

    protected InputManager _inputManager;
    protected InputManager inputManager
    {
        get
        {
            if (_inputManager == null)
            {
                _inputManager = Director.instance.inputManager;
            }
            return _inputManager;
        }
    }

    public override void Init()
    {
        base.Init();
        displayText = GetComponentInChildren<Text>();
        buttonImage = GetComponent<Image>();
        AddListener(ButtonFunction);
    }
    
    public void SetButtonText(string _text)
    {
        displayText.text = _text;
    }

    public void SetLisenter(UnityAction myEvent)
    {
        myButton.onClick.RemoveAllListeners();
        AddListener(myEvent);
    }

	public void AddListener(UnityAction myEvent) 
	{
        myButton.onClick.AddListener(myEvent);
	}

    public void RemoveListener(UnityAction myEvent)
    {
        myButton.onClick.RemoveListener(myEvent);
    }

	public void RemoveAllListener() 
	{
		myButton.onClick.RemoveAllListeners();
	}

    protected virtual void ButtonFunction()
    {
        ElementJellyEffect();
    }

    public void SetButtonColor(string hexColor)
    {
        buttonImage.color = Ultilities.HexToColor(hexColor);
    }
}
