﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjectButton : GameButton {

    [SerializeField]
    private ObjectType spawnedObjectType;

    [SerializeField]
    private TeamManager spawnedTeam; 

    protected override void ButtonFunction()
    {
        base.ButtonFunction();
        inputManager.SetSpawnObjectModule(spawnedObjectType, spawnedTeam);
        inputManager.OnRightClick += Director.instance.uiManager.HideEditorPanel;
    }

}
