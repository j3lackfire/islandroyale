﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnGunsButton : GameButton {

    [SerializeField]
    private WeaponType spawnedWeaponType;

    protected override void ButtonFunction()
    {
        base.ButtonFunction();
        inputManager.SetSpawnWeaponModule(spawnedWeaponType);
        inputManager.OnRightClick += Director.instance.uiManager.HideEditorPanel;
        inputManager.OnLeftClick += Director.instance.uiManager.HideEditorPanel;
    }
}
