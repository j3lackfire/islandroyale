﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorMenu : BaseUIElement {
    [SerializeField]
    private GameButton cancelAllButton;

    [ReadOnly][SerializeField]
    private List<UIFolderPanel> uiFolderPanels;

    public override void Init()
    {
        base.Init();

        cancelAllButton = transform.Find("CancelAllButton").GetComponent<GameButton>();
        cancelAllButton.AddListener(() => { Director.instance.uiManager.OnEditorHiden(); });

		uiFolderPanels = new List<UIFolderPanel>();
		uiFolderPanels.AddRange(GetComponentsInChildren<UIFolderPanel>());
    }

    public void ShowController()
    {
        gameObject.SetActive(true);
        foreach(UIFolderPanel uiFolder in uiFolderPanels)
        {
            uiFolder.OnElementShow();
        }
        cancelAllButton.OnElementShow();
    }

    public void HideControllerPanel()
    {
        this.gameObject.SetActive(false);
        foreach(UIFolderPanel u in uiFolderPanels)
        {
            u.FoldAllElement();
        }
    }

    public void HideAllPanelBut(UIFolderPanel panel)
    {
        foreach (UIFolderPanel uiFolder in uiFolderPanels)
        {
            if (uiFolder == panel)
            {
                continue;
            }
            uiFolder.FoldAllElement();
        }
    }
}
