﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjectPanel : UIFolderPanel {

    [SerializeField]
    private GameButton spawnButton;

    [SerializeField]
    private BaseUIElement spawnButtonsGroup;

    [SerializeField]
    private GameButton spawnSoldier;

    [SerializeField]
    private GameButton spawnSkeleton;

    private bool isSpawnButtonsShow;

    public override void Init()
    {
        base.Init();
        inputManager = Director.instance.inputManager;

		spawnButton = transform.Find("Show_Hide_Button").GetComponent<GameButton>();
		spawnButtonsGroup = transform.Find("ButtonsFolder").GetComponent<BaseUIElement>();
		spawnSoldier = spawnButtonsGroup.transform.Find("SpawnSoldier").GetComponent<GameButton>();
		spawnSkeleton = spawnButtonsGroup.transform.Find("SpawnSkeleton").GetComponent<GameButton>();

        spawnButton.AddListener(OnSpawnButtonClicked);

        spawnSoldier.AddListener(() =>
        {
            inputManager.SetSpawnObjectModule(ObjectType.Soldier_76, Director.instance.playerManager);
            inputManager.OnRightClick += Director.instance.uiManager.HideEditorPanel;
            OnButtonPressed();
        });

        spawnSkeleton.AddListener(() =>
        {
            inputManager.SetSpawnObjectModule(ObjectType.Undead_Skeleton, Director.instance.aiManager);
            inputManager.OnRightClick += Director.instance.uiManager.HideEditorPanel;
            OnButtonPressed();
        });
    }

    protected override void ShowElement()
    {
        base.ShowElement();
        isSpawnButtonsShow = false;
        spawnButtonsGroup.DeactiveElement();
    }

    private void OnSpawnButtonClicked()
    {
        if (isSpawnButtonsShow)
        {
            isSpawnButtonsShow = false;
            spawnButtonsGroup.OnElementHide();
        }
        else
        {
            isSpawnButtonsShow = true;
            spawnButtonsGroup.OnElementShow();
        }
    }

    //if the button is pressed, when clickin on the cancel button or back button, we need to 
    //change the mouse state.
    private void OnButtonPressed()
    {
        spawnButton.AddListener(OnSpawnButtonPressed_WhileSpawnObjectMode);
        uiManager.OnEditorHiden += OnControllerHiden_WhileSpawnObjectMode;
    }

    private void OnSpawnButtonPressed_WhileSpawnObjectMode()
    {
        inputManager.SetInputModule(ModuleInputType.Select_Object);
        spawnButton.RemoveListener(OnSpawnButtonPressed_WhileSpawnObjectMode);
        uiManager.OnEditorHiden -= OnControllerHiden_WhileSpawnObjectMode;
    }

    private void OnControllerHiden_WhileSpawnObjectMode()
    {
        inputManager.SetInputModule(ModuleInputType.Select_Object);
        spawnButton.RemoveListener(OnSpawnButtonPressed_WhileSpawnObjectMode);
        uiManager.OnEditorHiden -= OnControllerHiden_WhileSpawnObjectMode;
    }
}
