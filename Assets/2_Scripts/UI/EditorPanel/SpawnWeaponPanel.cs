﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnWeaponPanel : BaseUIElement {
	private InputManager inputManager;

	[SerializeField]
	private GameButton showHideButton;

	[SerializeField]
	private BaseUIElement spawnButtonsGroup;

	[SerializeField]
	private GameButton spawnPistol;

	[SerializeField]
	private GameButton spawnAK;

	[SerializeField]
	private GameButton spawnM4;

	private bool isSpawnButtonsShow;

	public override void Init()
	{
		base.Init();
		inputManager = Director.instance.inputManager;

		showHideButton.AddListener(OnSpawnButtonClicked);

		spawnPistol.AddListener(() =>
		{
			inputManager.SetSpawnWeaponModule(WeaponType.Pistol_Glock);
			inputManager.OnRightClick += Director.instance.uiManager.HideEditorPanel;
			OnButtonPressed();
		});

		spawnAK.AddListener(() =>
		{
			inputManager.SetSpawnWeaponModule(WeaponType.Rifle_AK);
			inputManager.OnRightClick += Director.instance.uiManager.HideEditorPanel;
			OnButtonPressed();
		});
		
		spawnM4.AddListener(() =>
		{
			inputManager.SetSpawnWeaponModule(WeaponType.Rifle_M4);
			inputManager.OnRightClick += Director.instance.uiManager.HideEditorPanel;
			OnButtonPressed();
		});
		
	}

	protected override void ShowElement()
	{
		base.ShowElement();
		isSpawnButtonsShow = false;
		spawnButtonsGroup.DeactiveElement();
	}

	private void OnSpawnButtonClicked()
	{
		if (isSpawnButtonsShow)
		{
			isSpawnButtonsShow = false;
			spawnButtonsGroup.OnElementHide();
		}
		else
		{
			isSpawnButtonsShow = true;
			spawnButtonsGroup.OnElementShow();
		}
	}

	//if the button is pressed, when clickin on the cancel button or back button, we need to 
	//change the mouse state.
	private void OnButtonPressed()
	{
		showHideButton.AddListener(OnSpawnButtonPressed_WhileSpawnObjectMode);
		uiManager.OnEditorHiden += OnControllerHiden_WhileSpawnObjectMode;
	}

	private void OnSpawnButtonPressed_WhileSpawnObjectMode()
	{
		inputManager.SetInputModule(ModuleInputType.Select_Object);
		showHideButton.RemoveListener(OnSpawnButtonPressed_WhileSpawnObjectMode);
		uiManager.OnEditorHiden -= OnControllerHiden_WhileSpawnObjectMode;
	}

	private void OnControllerHiden_WhileSpawnObjectMode()
	{
		inputManager.SetInputModule(ModuleInputType.Select_Object);
		showHideButton.RemoveListener(OnSpawnButtonPressed_WhileSpawnObjectMode);
		uiManager.OnEditorHiden -= OnControllerHiden_WhileSpawnObjectMode;
	}

}
