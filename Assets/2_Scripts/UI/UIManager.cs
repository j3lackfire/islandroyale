﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : BaseManager {
    //editor stuffs
    private GameButton showEditorMenuButton;
    private EditorMenu editorPanel;
    public VoidDelegate OnEditorHiden;

    //this function is called on Awake of Directors
	public override void Init()
    {
		base.Init();
        List<BaseUIElement> baseUIElement = transform.GetComponentsInDirectChildren<BaseUIElement>();
        for (int i = 0; i < baseUIElement.Count; i ++)
        {
            baseUIElement[i].Init();
        }
        List<SubUIManager> subUIManager = transform.GetComponentsInDirectChildren<SubUIManager>();
        for (int i = 0; i < subUIManager.Count; i++)
        {
            subUIManager[i].InitSubUIManager();
        }

        //editor stuffs
        editorPanel = transform.GetComponentInChildren<EditorMenu>();

        showEditorMenuButton = transform.Find("Editor").transform.Find("ShowEditorMenuButton").GetComponent<GameButton>();
		showEditorMenuButton.AddListener(ShowEditorPanel);

        OnEditorHiden = HideEditorPanel;
    }

	//to fix the bug that some UI components does not cache all the object neccessary
	private void Start() 
	{
		editorPanel.HideControllerPanel();
	}

    public EditorMenu GetEditorPanel() { return editorPanel; }

    private void ShowEditorPanel()
    {
        editorPanel.ShowController();
        showEditorMenuButton.DeactiveElement();
    }

    public void HideEditorPanel()
    {
        showEditorMenuButton.OnElementShow();
        editorPanel.HideControllerPanel();
    }
}
