﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Ultilities
{
    public static float fogMaskYPos = 2.55f;


    //randomly adjust value of a float by flutuation percent
    //For example. a float 100, will be randomly return from 90 to 110
    public static float DoFluctuation(this float number, float fluctuation)
    {
        return number * UnityEngine.Random.Range(1 - fluctuation, 1 + fluctuation);
    }

    //randomly return true or false
    public static bool RandomBoolean()
    {
        return UnityEngine.Random.Range(0, 2) == 0;
    }

    public static float GetDistanceBetween(GameObject first, GameObject second)
    {
        return ((first.transform.position - second.transform.position).magnitude);
    }

    //Get color from hex.
    public static Color HexToColor(string _hex)
    {
        if (_hex.Length != 6 && _hex.Length != 8)
        {
            Debug.LogError("<color=red>INVALID HEX TO COLOR VALUE !!!!  </color>" + _hex);
            return Color.black;
        }
        char[] stringChar = _hex.ToLower().ToCharArray();

        string r = stringChar[0].ToString() + stringChar[1].ToString();
        float red = (float)HexToInt(r) / 256f;

        string g = stringChar[2].ToString() + stringChar[3].ToString();
        float green = (float)HexToInt(g) / 256f;

        string b = stringChar[4].ToString() + stringChar[5].ToString();
        float blue = (float)HexToInt(b) / 256f;

        float alpha = 0;
        if (stringChar.Length == 8)
        {
            string a = stringChar[6].ToString() + stringChar[7].ToString();
            alpha = (float)HexToInt(a) / 256f;
        }
        else
        {
            alpha = 1f;
        }
        return new Color(red, green, blue, alpha);
    }

    public static string ColorToHex(Color color)
    {
        string returnString = "";
        returnString += FloatToHex(color.r);
        returnString += FloatToHex(color.g);
        returnString += FloatToHex(color.b);
        returnString += FloatToHex(color.a);

        return returnString;
    }

    private static int HexToInt(string _hex)
    {
        int returnInt = 0;
        char[] hexChar = _hex.ToCharArray();
        for (int i = hexChar.Length - 1; i > -1; i--)
        {
            returnInt += SingleHexCharToInt(hexChar[i]) * (int)Mathf.Pow(16f, (int)(hexChar.Length - 1 - i));
        }
        return returnInt;
    }

    private static int SingleHexCharToInt(char _hexCharacter)
    {
        switch (_hexCharacter)
        {
            case '0': return 0;
            case '1': return 1;
            case '2': return 2;
            case '3': return 3;
            case '4': return 4;
            case '5': return 5;
            case '6': return 6;
            case '7': return 7;
            case '8': return 8;
            case '9': return 9;
            case 'a': return 10;
            case 'b': return 11;
            case 'c': return 12;
            case 'd': return 13;
            case 'e': return 14;
            case 'f': return 15;
            default:
                Debug.LogError("<color=red>INVALID HEX TO INT VALUE !!!!  </color>" + _hexCharacter);
                return 0;
        }
    }

    //for color number, from 0 to 1
    public static string FloatToHex(float number)
    {
        if (number < 0f || number >= 1f)
        {
            Debug.LogError("<color=red> Invalid input FLOAT to Hex function</color> " + number);
            return "";
        }
        return IntToHex((int)(number * 255f));
    }

    //work for less than 255 --maybe someday I will write a function for it to work with bigger number
    public static string IntToHex(int number)
    {
        if (number < 0 || number > 255)
        {
            Debug.LogError("<color=red> Invalid input INT to Hex function</color> " + number);
            return "";
        }
        return SingleIntToString(number / 16) + SingleIntToString(number % 16);
    }

    private static string SingleIntToString(int number)
    {
        switch (number)
        {
            case 0:
                return "0";
            case 1:
                return "1";
            case 2:
                return "2";
            case 3:
                return "3";
            case 4:
                return "4";
            case 5:
                return "5";
            case 6:
                return "6";
            case 7:
                return "7";
            case 8:
                return "8";
            case 9:
                return "9";
            case 10:
                return "a";
            case 11:
                return "b";
            case 12:
                return "c";
            case 13:
                return "d";
            case 14:
                return "e";
            case 15:
                return "f";
            default:
                Debug.LogError("<color=red> Invalid input single INT to Hex function</color> " + number);
                return "";
        }
    }

    public static List<Transform> GetDirectChildrent(this Transform _transform)
    {
        List<Transform> returnTransform = new List<Transform>();
        for (int i = 0; i < _transform.childCount; i++)
        {
            returnTransform.Add(_transform.GetChild(i));
        }
        return returnTransform;
    }

    public static T GetComponentInDirectChildren<T>(this Transform _transform) where T : Component
    {
        for (int i = 0; i < _transform.childCount; i ++)
        {
            T temp = _transform.GetChild(i).GetComponent<T>();
            if (temp != null)
            {
                return temp;
            }
        }
        return null;
    }

    public static List<T> GetComponentsInDirectChildren<T>(this Transform _transform) where T : Component
    {
        List<T> returnT = new List<T>();
        for (int i = 0; i < _transform.childCount; i++)
        {
            T temp = _transform.GetChild(i).GetComponent<T>();
            if (temp != null)
            {
                returnT.Add(temp);
            }
        }
        return returnT;
    }
}
