﻿public enum ObjectState
{
    //Idle, //standing in one place and not doing anything
    //Run, //runnnnnn
    //Charge, //running to attack a target
    //Attack, //attack target
    //Stun, //object is stunned and can't do anything
    //Special, //is doing special spell
    //Die //die 

    Invalid = -1, //not valid
    Idle = 0, //idle, not doing anything, check for component
    Component = 1, //the main component is doing something :/
    Stun = 9, //is being stun or disabled
    Die = 10 //DIE

}