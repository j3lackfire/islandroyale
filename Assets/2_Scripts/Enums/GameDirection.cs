﻿public enum GameDirection{
    Invalid = -1,
    Up = 0,
    Down = 1,
    Left,
    Right,
    UpRight,
    UpLeft,
    DownLeft,
    DownRight
}
